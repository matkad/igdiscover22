"""
This provides functions for running the "igblastn" command-line tool
"""
import csv
import os
import shlex
import multiprocessing
import subprocess
from contextlib import ExitStack
from dataclasses import dataclass
from io import StringIO
import hashlib
from typing import Dict, Optional

import pkg_resources
import logging
import tempfile
import gzip
import math
import pandas as pd
import dnaio
import sys

from .utils import SerialPool, nt_to_aa
from .species import cdr3_start, cdr3_end


logger = logging.getLogger(__name__)


def escape_shell_command(command):
    return " ".join(shlex.quote(arg) for arg in command)


class IgBlastCache:
    """Cache IgBLAST results in ~/.cache"""

    binary = 'igblastn'

    def __init__(self):
        self._hasher = None
        cache_home = os.environ.get('XDG_CACHE_HOME', os.path.expanduser('~/.cache'))
        self._cachedir = os.path.join(cache_home, 'igdiscover')

    @property
    def cachedir(self):
        return self._cachedir

    def _path(self, digest):
        """Return path to cache file given a digest"""
        return os.path.join(self._cachedir, digest[:2], digest) + '.txt.gz'

    def _load(self, digest):
        try:
            with gzip.open(self._path(digest), 'rt') as f:
                return f.read()
        except FileNotFoundError:
            return None

    def _make_hasher(self):
        if self._hasher is None:
            version_string = subprocess.check_output([IgBlastCache.binary, '-version'])
            self._hasher = hashlib.md5(version_string)
        return self._hasher.copy()

    def retrieve(self, variable_arguments, fixed_arguments, blastdb_dir, fasta_str) -> str:
        hasher = self._make_hasher()
        hasher.update(' '.join(fixed_arguments).encode())
        for gene in 'V', 'D', 'J':
            with open(os.path.join(blastdb_dir, gene + '.fasta'), 'rb') as f:
                hasher.update(f.read())
        hasher.update(fasta_str.encode())
        digest = hasher.hexdigest()
        data = self._load(digest)
        if data is None:
            with tempfile.TemporaryDirectory() as tmpdir:
                path = os.path.join(tmpdir, 'igblast.txt')
                full_arguments = [IgBlastCache.binary] + variable_arguments + fixed_arguments\
                    + ['-out', path]
                output = subprocess.check_output(full_arguments, input=fasta_str,
                    universal_newlines=True)
                assert output == ''
                with open(path) as f:
                    data = f.read()

                compressed_path = os.path.join(tmpdir, "igblast.txt.gz")
                with gzip.open(compressed_path, 'wt') as f:
                    f.write(data)

                cache_path = self._path(digest)
                cache_dir = os.path.dirname(cache_path)
                os.makedirs(cache_dir, exist_ok=True)
                # Atomic move - hopefully this means we don’t need a lock
                os.rename(compressed_path, cache_path)

        return data


def run_igblast(
    sequences, blastdb_dir, species, sequence_type, penalty, cache: Optional[IgBlastCache] = None, aux=None
) -> str:
    """
    Run the igblastn command-line program.

    sequences -- list of Sequence objects
    blastdb_dir -- directory that contains BLAST databases. Files in that
    directory must be databases created by the makeblastdb program and have
    names V, D, and J.

    Return IgBLAST’s raw output as a string.
    """
    if sequence_type not in ('Ig', 'TCR'):
        raise ValueError('sequence_type must be "Ig" or "TCR"')
    variable_arguments = []
    for gene in 'V', 'D', 'J':
        variable_arguments += ['-germline_db_{gene}'.format(gene=gene),
            os.path.join(blastdb_dir, '{gene}'.format(gene=gene))]
    # An empty .aux suppresses a warning from IgBLAST. /dev/null does not work.
    if aux:
        variable_arguments += ['-auxiliary_data', os.path.join(blastdb_dir, 'temp.aux')]
    else:
        empty_aux = pkg_resources.resource_filename('igdiscover', 'empty.aux')
        variable_arguments += ['-auxiliary_data', empty_aux]
    arguments = []
    if penalty is not None:
        arguments += ['-penalty', str(penalty)]
    if species is not None:
        arguments += ['-organism', species]

    arguments += [
        '-ig_seqtype', sequence_type,
        '-num_threads', '1',
        '-domain_system', 'imgt',
        '-num_alignments_V', '1',
        '-num_alignments_D', '1',
        '-num_alignments_J', '1',
        '-outfmt', '19',  # AIRR format
        '-query', '-',
    ]
    fasta_str = ''.join(">{}\n{}\n".format(r.name, r.sequence) for r in sequences)

    if cache:
        return cache.retrieve(variable_arguments, arguments, blastdb_dir, fasta_str)
    else:
        # For some reason, it has become unreliable to let IgBLAST 1.10 write its result
        # to standard output using "-out -". The data becomes corrupt. This does not occur
        # when calling igblastn in the shell and using the same syntax, only with
        # subprocess.check_output. As a workaround, we write the output to a temporary file.
        with tempfile.TemporaryDirectory() as tmpdir:
            path = os.path.join(tmpdir, 'igblast.txt')
            command = ['igblastn'] + variable_arguments + arguments + ['-out', path]
            logger.debug("Running %s", escape_shell_command(command))
            output = subprocess.check_output(command, input=fasta_str, universal_newlines=True)
            assert output == ''
            with open(path) as f:
                return f.read()


def chunked(iterable, chunksize: int):
    """
    Group the iterable into lists of length chunksize
    >>> list(chunked('ABCDEFG', 3))
    [['A', 'B', 'C'], ['D', 'E', 'F'], ['G']]
    """
    chunk = []
    for it in iterable:
        if len(chunk) == chunksize:
            yield chunk
            chunk = []
        chunk.append(it)
    if chunk:
        yield chunk


class RawRunner:
    """
    This is the target of a multiprocessing pool. The target needs to
    be pickleable, and because nested functions cannot be pickled,
    we need this separate class.

    It runs IgBLAST and returns raw AIRR-formatted output
    """

    def __init__(
        self, blastdb_dir, species, sequence_type, penalty, database, cache, aux
    ):
        self.blastdb_dir = blastdb_dir
        self.species = species
        self.sequence_type = sequence_type
        self.penalty = penalty
        self.database = database
        self.cache = cache
        self.aux = aux

    def __call__(self, sequences):
        """
        Return raw IgBLAST output
        """
        return run_igblast(
            sequences,
            self.blastdb_dir,
            self.species,
            self.sequence_type,
            self.penalty,
            self.cache,
            aux=self.aux,
        )


class MakeBlastDbError(subprocess.CalledProcessError):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return f"Running '{escape_shell_command(self.cmd)}' failed with " \
               f"exit code {self.returncode}. " \
               f"Standard output:\n{self.output.decode()}\n" \
               f"Standard error:\n{self.stderr.decode()}"


def makeblastdb(fasta, database_name, prefix=''):
    """
    prefix -- prefix to add to sequence ids
    """
    n = 0
    with dnaio.open(fasta) as fr, open(database_name + ".fasta", "w") as db:
        for record in fr:
            name = prefix + record.name.split(maxsplit=1)[0]
            db.write(">{}\n{}\n".format(name, record.sequence))
            n += 1
    if n == 0:
        raise ValueError("FASTA file {} is empty".format(fasta))

    command = [
        'makeblastdb', '-parse_seqids', '-dbtype', 'nucl', '-in', database_name + ".fasta", '-out',
        database_name
    ]
    logger.debug("Running %s", escape_shell_command(command))
    try:
        process_output = subprocess.check_output(command, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        raise MakeBlastDbError(e.returncode, command, output=e.output, stderr=e.stderr) from None

    if b'Error: ' in process_output:
        raise MakeBlastDbError(0, command, stderr=process_output) from None


class Database:
    def __init__(self, path, sequence_type):
        """path -- path to database directory with V.fasta, D.fasta, J.fasta"""
        self.path = path
        self.sequence_type = sequence_type
        self._v_records = self._read_fasta(os.path.join(path, 'V.fasta'))
        self.v = self._records_to_dict(self._v_records)
        self._d_records = self._read_fasta(os.path.join(path, 'D.fasta'))
        self.d = self._records_to_dict(self._d_records)
        self._j_records = self._read_fasta(os.path.join(path, 'J.fasta'))
        self.j = self._records_to_dict(self._j_records)
        self._cdr3_starts = dict()
        self._cdr3_ends = dict()
        for locus in ("IGH", "IGK", "IGL", "TRA", "TRB", "TRG", "TRD"):
            self._cdr3_starts[locus] = {name: cdr3_start(s, locus) for name, s in self.v.items()}
            self._cdr3_ends[locus] = {name: cdr3_end(s, locus) for name, s in self.j.items()}
        self.v_regions_nt, self.v_regions_aa = self._find_v_regions()

    @staticmethod
    def _read_fasta(path):
        records = []
        with dnaio.open(path) as sr:
            for record in sr:
                record.name = record.name.split(maxsplit=1)[0]
                records.append(record)
        return records

    @staticmethod
    def _records_to_dict(records):
        return {record.name: record.sequence.upper() for record in records}

    def v_cdr3_start(self, gene, locus):
        return self._cdr3_starts[locus][gene]

    def j_cdr3_end(self, gene, locus):
        return self._cdr3_ends[locus][gene]

    def _find_v_regions(self):
        """
        Run IgBLAST on the V sequences to determine the nucleotide and amino-acid sequences of the
        FR1, CDR1, FR2, CDR2 and FR3 regions
        """
        v_regions_nt = dict()
        v_regions_aa = dict()
        for record in igblast_records(self.path, self._v_records, self.sequence_type):
            nt_regions = dict()
            aa_regions = dict()
            for region in ('FR1', 'CDR1', 'FR2', 'CDR2', 'FR3'):
                nt_seq = record.region_sequence(region)
                if nt_seq is None:
                    break
                if len(nt_seq) % 3 != 0:
                    logger.warning('Length %s of %s region in %r is not divisible by three; region '
                        'info for this gene will not be available',
                        len(nt_seq), region, record.query_name)
                    # not codon-aligned, skip entire record
                    break
                nt_regions[region] = nt_seq
                try:
                    aa_seq = nt_to_aa(nt_seq)
                except ValueError as e:
                    logger.warning('The %s region could not be converted to amino acids: %s',
                        region, str(e))
                    break
                if '*' in aa_seq:
                    logger.warning('The %s region in %r contains a stop codon (%r); region info '
                        'for this gene will not be available',
                        region, record.query_name, aa_seq)
                    break
                aa_regions[region] = aa_seq
            else:
                v_regions_nt[record.query_name] = nt_regions
                v_regions_aa[record.query_name] = aa_regions

        return v_regions_nt, v_regions_aa


def igblast_records(database, sequences, sequence_type, species=None, penalty=None,
        use_cache=False):
    """
    Run IgBLAST, parse results and yield RegionRecords objects.

    database -- Path to database directory with V./D./J.fasta files
    sequences -- an iterable of Sequence objects
    sequence_type -- 'Ig' or 'TCR'
    """
    # Create the BLAST databases in a temporary directory
    with tempfile.TemporaryDirectory() as blastdb_dir:
        make_vdj_blastdb(blastdb_dir, database)
        igblast_result = run_igblast(sequences, blastdb_dir, species, sequence_type, penalty, use_cache)
        sio = StringIO(igblast_result)
        for record in parse_region_records(sio):
            yield record


def parse_region_records(file):
    csv.register_dialect(
        "airr",
        delimiter="\t",
        lineterminator="\n",
        strict=True,
    )
    reader = csv.DictReader(file, dialect="airr")
    for record in reader:
        yield RegionsRecord(
            query_name=record["sequence_id"],
            fields=record,
        )


@dataclass
class RegionsRecord:
    query_name: str
    fields: Dict[str, str]

    COLUMNS_MAP = {
        "CDR1": "cdr1",
        "CDR2": "cdr2",
        "FR1": "fwr1",
        "FR2": "fwr2",
        "FR3": "fwr3",
    }

    def region_sequence(self, region: str) -> str:
        """
        Return the nucleotide sequence of a named region. Allowed names are:
        CDR1, CDR2, CDR3, FR1, FR2, FR3. Sequences are extracted from the full read
        using begin and end coordinates from IgBLAST’s "alignment summary" table.
        """
        if region not in self.COLUMNS_MAP:
            raise KeyError(f"Region '{region}' not allowed")
        return self.fields[self.COLUMNS_MAP[region]]


def igblast_parallel_chunked(
    database,
    sequences,
    sequence_type,
    species=None,
    threads=1,
    penalty=None,
    cache=None,
    aux=None
):
    """
    Run IgBLAST on the input sequences and yield AIRR-formatted results.

    The input is split up into chunks of 1000 sequences and distributed to
    *threads* number of IgBLAST instances that run in parallel.

    database -- Path to database directory with V./D./J.fasta files
    sequences -- an iterable of Sequence objects
    sequence_type -- 'Ig' or 'TCR'
    threads -- number of threads.
    """
    with ExitStack() as stack:
        # Create the three BLAST databases in a temporary directory
        blastdb_dir = stack.enter_context(tempfile.TemporaryDirectory())
        make_vdj_blastdb(blastdb_dir, database)
        if aux:
            update_aux_file(blastdb_dir, aux)
        chunks = chunked(sequences, chunksize=1000)
        runner = RawRunner(
            blastdb_dir,
            species=species,
            sequence_type=sequence_type,
            penalty=penalty,
            database=database,
            cache=cache,
            aux=aux,
        )
        pool = stack.enter_context(
            multiprocessing.Pool(threads) if threads > 1 else SerialPool()
        )
        for igblast_output in pool.imap(runner, chunks, chunksize=1):
            yield igblast_output


def update_aux_file(blastdb_dir, aux_path, prefix = "%"):
    """If a novel J has been inferred, give it the same aux values as its source allele."""
    aux = pd.read_csv(os.path.join(aux_path) ,
                sep = "\s+", comment='#', skip_blank_lines=True, header = None)
    aux = aux.rename(columns = {0:"j_call"})
    aux['j_call'] = aux['j_call'].apply(lambda x: prefix + x)
    with dnaio.open(os.path.join(blastdb_dir, 'J.fasta')) as fr:
        J_db_names = [record.name for record in fr]
    unknown_js = set(J_db_names).difference(set(aux['j_call'].values))
    novel_unknowns = [j_call for j_call in unknown_js if "_S" in j_call]
    if len(novel_unknowns) != len(unknown_js):
        logger.error('Could not obtain auxiliary values found for alleles %s . '
        'This means IgBLAST will not be able to find cdr3s for these '
        'alleles. Please add them to %s and execute `igdiscover run`',
        ", ".join([j_call[len(prefix):] for j_call in list(set(unknown_js).difference(set(novel_unknowns)))]),
        aux_path)
        sys.exit(1)
    for unknown_j in novel_unknowns:
        s = unknown_j.split("_S")
        #novel allele with _S
        if len(s) == 2:
            r = aux[aux['j_call'] == s[0]]
            if r is not None and len(r) != 0: 
                if isinstance(r, pd.DataFrame):
                    r = r.iloc[0]
                r['j_call'] = unknown_j
                aux = pd.concat([aux, pd.DataFrame(r).transpose()], join='inner', ignore_index=True)
    aux = aux.sort_index().reset_index(drop=True)
    blastdb_aux = os.path.join(blastdb_dir, "temp.aux")
    write_aux(blastdb_aux, aux)
    return blastdb_aux

def write_aux(aux, df):
    def convert_aux(x):
        if isinstance(x, str) or math.isnan(x):
            return x
        else:
            return int(x)
    if os.path.isfile(aux):
        os.remove(aux)
    open(aux, 'a').close()
    for i in range(len(df)):
        pd.DataFrame(df.iloc[i,:].apply(convert_aux)).transpose().to_csv(aux,
                                                                        header=None,
                                                                        sep = "\t",
                                                                        index = False,
                                                                        mode = 'a')


def make_vdj_blastdb(blastdb_dir, database_dir, prefix = "%"):
    """Run makeblastdb for all {V,D,J}.fasta in the database_dir"""

    for gene in ["V", "D", "J"]:
        # Without adding the "%" prefix, IgBLAST reports record names that look like GenBank
        # ids as "gb|original_name|".
        makeblastdb(
            os.path.join(database_dir, gene + ".fasta"),
            os.path.join(blastdb_dir, gene),
            prefix="%",
        )
