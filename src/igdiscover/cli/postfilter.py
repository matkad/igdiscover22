"""
Filter V genes from germline.tab
"""
from timeit import repeat
import pandas as pd
import numpy as np
import json
import os.path
import os
from . import CommandLineError

def read_expected(path):
    try:
        with open(path) as f:
            pass
    except OSError as e:
        raise CommandLineError(f'Could not open {path!r}: {e}')
    return pd.read_csv(path, sep='\t', dtype=str)

def add_arguments(parser):
    arg = parser.add_argument
    arg('germline', metavar='GERMLINE.TSV',
        help='Germline tables (one or more) created by the "discover" command',
        nargs='+')
    arg('--expected', type=str,
            help='TSV file with expected Vs frequencies')
    arg('--allelic-groups', metavar='JSON', default=None,
        help='Dictionary with initial V names and recomputed allelic group names.')
    arg('--fasta', type=str,
            help='File to save sequences')

def main(args):
    # Load and combine
    tables = []
    for path in args.germline:
        germline = pd.read_table(path)
        tables.append(germline)
    table = pd.concat(tables).sort_values('name')
    table = table.drop_duplicates('name')  # Drop duplicates after combining
    table = table.assign(full_exact_ratio=table.full_exact / table.exact)

    # Default is unfiltered combined
    filtered = table
    if args.expected:
        expected = pd.read_csv(args.expected, sep='\t')
        assert all(expected.columns.isin(['gene','frequency'])), "Required columns missing"
        if args.allelic_groups:
            with open(args.allelic_groups) as fr:
                allelic_groups = json.load(fr)
            expected['gene'] = expected['gene'].replace(allelic_groups)
        threshold = dict(expected[['gene','frequency']].values)
        keep = []
        # Iterate rows and apply threshold
        total_exact = table['barcodes_exact'].sum()
        for _,row in table.iterrows():

            if args.allelic_groups == '':
                gene = row['name'].rsplit('*',1)[0]
            else:
                gene = row['name'].rsplit('@',1)[0]

            t = threshold.get(gene, 0)
            v = row['barcodes_exact'] / total_exact
            if v > t:
                keep.append(row)
        # Update default table
        filtered = pd.DataFrame(keep)

    print(filtered.to_csv(sep='\t', index=False))
    if args.fasta:
        with open(args.fasta, 'w') as f:
                for _, row in filtered.iterrows():
                        print('>{}\n{}'.format(row['name'], row['consensus']), file=f)
