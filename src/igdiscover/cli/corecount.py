"""
Core-count module for IgDiscover.

Searches filtered or assigned table for trimmed exact matches.

"""
import logging
import json
import pandas as pd
import numpy as np
from Bio import SeqIO
import argparse
from collections import Counter
from tqdm import tqdm

from ..table import fix_columns
from ..table import read_table

logger = logging.getLogger(__name__)

CORECOUNT_COLUMNS = ["sequence_id", "sequence", "v_call", "d_call", "j_call", "np1", "np2", "D_region", 
                    "V_errors", "D_errors", "J_errors", "V_covered", "D_covered", "J_covered", "stop_codon",
                    "cdr3", "barcode","V_nt","VDJ_nt"]

def add_arguments(parser):
    arg = parser.add_argument
    arg("-g", "--gene", type=str, choices=['V', 'D', 'J'], default='V',
                            help="Required for selecting right column from filtered.tab")
    arg('fastadb', help='Fasta file to build core sequences from')
    arg('filtered', help='File in filtered.tab.gz format from IgDiscover')
    arg("-s", "--segment", type=str, choices=['V_nt', 'VDJ_nt', 'VDJ_junction', 'sequence'], default='sequence', help='Which column to use for searching: V_nt is V nucleotide sequence, VDJ_nt is VDJ nucleotide sequence, VDJ_junction is VJ_juntion region + D_region + DJ_junction region combined, sequence is full read sequence')
    arg('output', help='Output file where to store summary')
    arg("-u", "--uout", type=str, help='Output matching alleles before final filters (i.e allelic ratio, max length CDR3...)')
    arg("-e", "--pend", type=int, default=-1, help='How much to trim at 3 prime end (default gene preset)')
    arg("-b", "--pbegin", type=int, default=-1, help='How much to trim at 5 prime begining (default gene preset)')
    arg("-m", "--mincount", type=int, default=1, help='Minimum of core counts (default 1)')
    arg("-r", "--ratio", type=float, default=0.1,
                            help='Type of allelic ratio filter, applied within each gene on core counts (default 0.1)')
    arg("-l", "--lenmaxfreq", type=float, default=0.8,
                            help='Sequences must have at most this many unique CDR3s with the same length (default 0.8).')
    arg("-L", "--seqmaxfreq", type=float, default=1.0,
                            help='Sequences must have at most this many unique CDR3s (default 1.0).')
    arg("-v", "--vfasta", type=str,help='Fasta file with V allele names to limit counting to.')
    arg("-j", "--jfasta", type=str,help='Fasta file with J allele names to limit counting to.')
    arg("-d", "--dfasta", type=str,help='Fasta file with D allele names to limit counting to.')
    arg("-f","--expected",type=str, help="Table with lower count frequency limits per gene or allele. Required columns are: ['gene', 'frequency']. If additional frequency_only column is set to True, all filters other than the frequency will be bypassed.")
    arg("-x","--strict-filtering", action='store_true', default=False, help="Enable filtered table pre-filtering (V/Jerrors,V/Jcoverage,stop-codon) to customize these filters, see options below:")
    arg("--v-covered", type=float, default=0.99, help="Required V coverage (0.99).")
    arg("--j-covered", type=float, default=0.95, help="Required J coverage (0.95).")
    arg("--v-errors", type=int, default=3, help="Maximum V errors (3).")
    arg("--j-errors", type=int, default=3, help="Maximum J errors (3).")
    arg("--j-anchor",type=str, help="Coma separated pair of two alleles to use for J haplotyping.")
    arg("--v-anchor",type=str, help="Coma separated pair of two alleles to use for V haplotyping.")
    arg("--trim-table",type=str, help="Table with gene names and corresponding begin and end trims, if specified thes override defaults. Columns are: ['gene','begin','end'].")
    arg('--allelic-group', action='append',
        help='Group of genes which should be treated as one. Default: IGHV3-30,IGHV3-30-3,IGHV3-30-5. Disable by providing empty string "".')
    arg('--lcs', action='store_true', help="Use longest common substring (LCS) for searching for allele unique streches instead of fixed length exact search.")
    arg('--lcs-coverage', type=float, default=0.6, help="Minimum coverage of substring with respect to database allele for LCS counting.")
    arg('--lcs-ignore', action='append', default=[],
        help='Set of alleles which LCS ignores. Default disabled. Use this option multiple times for multiple alleles.')
    arg('--verbose', action='store_true', help="Show more verbose messages.")

"""
Function to compute all longest common substrings between two strings
"""
def lcs(S,T):
    m = len(S)
    n = len(T)
    counter = [[0]*(n+1) for x in range(m+1)]
    longest = 0
    lcs_set = set()
    for i in range(m):
        for j in range(n):
            if S[i] == T[j]:
                c = counter[i][j] + 1
                counter[i+1][j+1] = c
                if c > longest:
                    lcs_set = set()
                    longest = c
                    lcs_set.add(S[i-c+1:i+1])
                elif c == longest:
                    lcs_set.add(S[i-c+1:i+1])
    return lcs_set

"""
Function counts occurences of specific sequence in dictionary of D sequences
"""
def find_match(d_seq, Ds):
    count = 0
    for (name,seq) in Ds.items():
        if d_seq in seq:
            count += 1
    return count

"""
Function counts longest common substrings that are unique in database of D sequences and
above specified coverage with respect length of matching database sequence
"""
def find_lcs_match(d_seq, target_D, Ds, lcs_coverage=0.6):
    found = ""
    common = lcs(target_D, d_seq)
    seq_len = len(target_D)
    for substring in common:
        substring_len = len(substring)
        ratio = substring_len / seq_len
        if ratio >= lcs_coverage:
            count = find_match(substring, Ds) 
            if count == 1:
                found = substring
    return found

"""
If expected frequency table is provied, filter with provided limits
"""
def filter_by_expected(table, expected):
    allele_select = expected['gene'].apply(lambda x: "*" in x)
    allele_expected = expected.loc[allele_select,:]
    gene_expected = expected.loc[~allele_select,:]
    
    # Combine genes and alleles
    gene_frequency_table = pd.merge(table,gene_expected,on='gene', how='left')
    allele_frequency_table = pd.merge(table,allele_expected,left_on='name', right_on = "gene", how='left')
    # Allele level is higher priority than gene level
    ftable = allele_frequency_table.combine_first(gene_frequency_table)

    # Select only genes that are limited by expected
    limited = ftable[~ftable.frequency.isna()]
    # Select only those that pass filter
    select = limited.apply(lambda x: x['count_frequency']>=x['frequency'],axis=1)
    # What to discard
    discard = limited.loc[~select, "name"].unique()
    # Report
    for name, count_frequency in zip(limited.loc[~select, "name"],limited.loc[~select, "count_frequency"]):
        logger.warning("\033[1;41mAllele {} not passing frequency filter with {:.6f} count_frequency.\033[1;0m".format(name, count_frequency))
    # Return filtered table and discarded alleles
    return table[~table['name'].isin(discard)]

"""
Compute useful statistics for candidates
"""
def stats(filtered, match):
    # Total count
    count = match.sum()
    # Unique barcodes
    barcodes = filtered.barcode[match].nunique()
    # CDR3
    total_cdr3 = filtered.cdr3[match].count()
    cdr3s = filtered.cdr3[match].dropna()
    if cdr3s.count() > 0:
        most_common_cdr3_len = Counter(cdr3s.str.len()).most_common(1)[0][1]
        most_common_cdr3_seq = Counter(cdr3s).most_common(1)[0][1]
    else:
        most_common_cdr3_len = 0
        most_common_cdr3_seq = 0
    cdr3_maxlen_freq = 0
    cdr3_maxseq_freq = 0
    if total_cdr3 > 0:
        cdr3_maxlen_freq = most_common_cdr3_len / total_cdr3
        cdr3_maxseq_freq = most_common_cdr3_seq / total_cdr3
    # Count Js
    js = dict(Counter(filtered.j_call[match]))
    vs = dict(Counter(filtered.v_call[match]))

    # Return
    return count, barcodes, cdr3_maxlen_freq, cdr3_maxseq_freq, js, vs

"""
Search assignments table for trimmed sequences or longest common substring and return matches
"""
def search(filtered, db, trimmed, full_db, gene, segment, vused, dused, jused, v_anchor, j_anchor, lcs, lcs_ignore, lcs_coverage=0.6, verbose=False):
    matches = {}
    if gene == 'D':
        logger.info("D gene selected, using VDJ_junction segment.")
        segment = 'VDJ_junction'
    if lcs:
        logger.info(f"LCS ignoring: {','.join(lcs_ignore)}")
    for name, core in tqdm(db.items()):
        if lcs & (name not in set(lcs_ignore)):
            substring = [find_lcs_match(s, core, full_db, lcs_coverage=lcs_coverage) for s in filtered[segment]]
            if verbose:
                logger.info(f"Name {name} core {core}")
                logger.info(f"{Counter([m for m in substring if m != '']).most_common(10000)}")
            filtered = filtered.assign(substring=substring)
            match = filtered.substring != ""
        else:
            match = filtered[segment].str.find(core) > -1

        if match.sum() > 0:
            count, barcodes, cdr3_maxlen_freq, cdr3_maxseq_freq, js, vs = stats(filtered, match)
            matches.setdefault('name',[]).append(name)
            matches.setdefault('count',[]).append(count)
            matches.setdefault('barcodes',[]).append(barcodes)
            matches.setdefault('cdr3_maxlen_freq',[]).append(cdr3_maxlen_freq)
            matches.setdefault('cdr3_maxseq_freq',[]).append(cdr3_maxseq_freq)
            matches.setdefault('core_seq',[]).append(core)
            matches.setdefault('nt_trimmed',[]).append(trimmed[name])

            if len(v_anchor) == 2:
                x,y = v_anchor
                x_count = (filtered[match].v_call == x).sum()
                y_count = (filtered[match].v_call == y).sum()
                matches.setdefault(x,[]).append(x_count)
                matches.setdefault(y,[]).append(y_count)
            if len(j_anchor) == 2:
                x,y = j_anchor
                x_count = (filtered[match].j_call == x).sum()
                y_count = (filtered[match].j_call == y).sum()
                matches.setdefault(x,[]).append(x_count)
                matches.setdefault(y,[]).append(y_count)

            if gene in ['V','D']:
                # Count Js
                for j in jused:
                    matches.setdefault(j,[]).append(js.get(j, 0))
            if gene in ['D','J']:
                # Count Vs
                for v in vused:
                    matches.setdefault(v, []).append(vs.get(v, 0))

    # Make sure there are no NA labels for the current gene for which we compute total counts
    # This was an issue wth Ds which we are not prefiltering
    nona = filtered[gene.lower()+'_call'].isna()
    filtered = filtered[~nona]

    # Add total counts
    counts = filtered.groupby(filtered[gene.lower()+'_call'].apply(lambda x: x.split('*')[0])).size().to_frame(gene.lower()+'_gene_count').reset_index()
    matches = pd.DataFrame.from_dict(matches)
    if len(matches) == 0:
        return matches
    matches = matches.assign(gene=matches.name.apply(lambda x: x.split('*')[0]))
    matches = pd.merge(matches, counts, left_on='gene',right_on=gene.lower()+'_call',how='left').drop(gene.lower()+'_call',axis=1)
    return matches

"""
Add allelic groups and frequencies later used for filtering
"""
def compute_frequencies(matches, allelic_groups):
    # Get grouping by gene
    matches['gene'] = matches.name.apply(lambda x: x.rsplit('*', 1)[0])
    matches['allelic_group'] = matches['gene']

    groups = []
    for i,allelic_group in enumerate(allelic_groups):
        for allele in allelic_group.split(','):
            group_name = 'IGHV-G{}'.format(i)
            logger.info("Assigning allelic group {} to allele {}".format(group_name, allele))
            matches.loc[matches['gene'] == allele, 'allelic_group'] = group_name
    for gene, group in matches.groupby('allelic_group'):
        ratios = group['count'] / group['count'].sum()
        group = group.assign(ratio=ratios)
        groups.append(group)
    return pd.concat(groups)

"""
Filter by parametrized criteria
"""
def filter(matches, ratio, mincount=1, cdr3_maxlen_freq=0.8, cdr3_maxseq_freq=1.0):
    ratio_filtered = []
    for gene, group in matches.groupby('allelic_group'):
        # Check if to apply mincount filter
        if mincount:
            ratio_filtered.append(group[(group.ratio > ratio) & (group['count'] > mincount)])
        else:
            ratio_filtered.append(group[(group.ratio > ratio)])
    filtered = pd.concat(ratio_filtered)
    filtered = filtered[filtered.cdr3_maxlen_freq <= cdr3_maxlen_freq]
    filtered = filtered[filtered.cdr3_maxseq_freq <= cdr3_maxseq_freq]
    return filtered

"""
Load database from fasta file into dictionary
"""
def load_db(path):
    db = {}
    for record in SeqIO.parse(path,'fasta'):
        db.setdefault('seqname',[]).append(record.name)
        db.setdefault('sequence',[]).append(str(record.seq).upper())
    return pd.DataFrame.from_dict(db)

def trimmer(sequence, sequences, ntrim=3, end=True):
    trimmed = sequence[0:len(sequence)-ntrim]
    if not end:
        trimmed = sequence[ntrim:len(sequence)]
    found = len([s for s in sequences if trimmed in s])
    if ntrim == 0:
        # No solution found, return immedietly to prevent infinite recurrsion
        return sequence,ntrim
    # Update ntrim - step back
    if found > 1:
        trimmed,ntrim = trimmer(sequence, sequences, ntrim-1,end=end)
    return trimmed,ntrim

def sequence_trim(db_df,ntrim,end=True):
    trims = []
    new_trimmed = list(db_df.sequence)
    for sequence in db_df.sequence:
        trimmed_seq, trim = trimmer(sequence,new_trimmed, ntrim, end=end)
        trims.append(trim)
        new_trimmed.append(trimmed_seq)

    db_df = db_df.assign(trims=trims)

    groups = []
    for gene,group in db_df.assign(gene=db_df.seqname.str.split('*',n=1).str[0]).groupby('gene'):
        # Update trimming to be only minimum per gene group
        groups.append(group.assign(trims=group.trims.min()))
    trimmed_df = pd.concat(groups)
    trimmed_df = trimmed_df.assign(sequence_trimmed=trimmed_df.apply(lambda x: x['sequence'][0:len(x['sequence'])-x['trims']],axis=1))
    # Report warnings
    for _,row in trimmed_df.iterrows():
        if row['trims'] != ntrim:
            if end:
                logger.warning("\033[1;31mAllele {} cannot be trimmed more than {} from 3'\033[1;0m".format(row['seqname'],row['trims']))
            else:
                logger.warning("\033[1;31mAllele {} cannot be trimmed more than {} from 5'\033[1;0m".format(row['seqname'],row['trims']))
    if not end:
        trimmed_df = trimmed_df.assign(sequence_trimmed=trimmed_df.apply(lambda x: x['sequence'][x['trims']:len(x['sequence'])],axis=1))
    return trimmed_df

def trim_Ds(db_df, begin_trim, end_trim):
    logger.info("Trimming D database")
    trimmed_df = sequence_trim(db_df, end_trim, end=True)
    trimmed_df = trimmed_df[['seqname','sequence_trimmed']].rename({'sequence_trimmed':'sequence'},axis=1)
    trimmed_df = sequence_trim(trimmed_df,begin_trim,end=False)
    return trimmed_df

def trim_Vs(db_df, end_trim):
    trimmed_df = sequence_trim(db_df, end_trim, end=True)
    return trimmed_df

def trim_Js(db_df, begin_trim):
    trimmed_df = sequence_trim(db_df, begin_trim, end=False)
    return trimmed_df

def main(args):
    # Handle allelic groups, useful if multiple genes are suspected to be the same gene
    if args.allelic_group:
        if len(args.allelic_group[0]) != 0:
            logger.info("Provided allelic groups:")
            for i,allelic in enumerate(args.allelic_group):
                logger.info("Group {}: {}".format(i,allelic))
        else:
            logger.info("Allelic groups disabled.")
    else:
        args.allelic_group = ['IGHV3-30,IGHV3-30-3,IGHV3-30-5']
        logger.info("Default allelic groups:")
        for i,allelic in enumerate(args.allelic_group):
            logger.info("Group {}: {}".format(i,allelic))

    # Default trimming
    begin = 0
    end = 2
    if args.gene == 'D':
        begin = 2
        end = 2
    elif args.gene == 'V':
        begin = 2
        end = 0

    # If to override presets
    if args.pend >= 0:
        end = args.pend
    if args.pbegin >= 0:
        begin = args.pbegin

    logger.info(f"Using begin {begin} and end {end}.")
    j_anchor, v_anchor = [], []
    if args.j_anchor:
        j_anchor = args.j_anchor.split(',')[0:2]
    if args.v_anchor:
        v_anchor = args.v_anchor.split(',')[0:2]

    # Load and trim core sequences
    db_df = load_db(args.fastadb)

    # We drop duplicates
    dups = set(db_df[db_df.sequence.duplicated(keep=False)].seqname)
    db_df = db_df.drop_duplicates('sequence',keep='first')
    dropped = dups - set(db_df.seqname)
    if len(dropped) > 0:
        logger.warning("Found duplicates, dropped first occurence: {}".format(','.join(dropped)))

    # Load trim table
    trim_table_dict = {}
    if args.trim_table:
        assert ((begin == 0) & (end == 0) & ~(args.lcs)), "For manual table trimming global (begin/end) trimmings must be zero and lcs must be disabled."
        trim_table = pd.read_csv(args.trim_table, sep='\t')
        assert all(trim_table.columns.isin(['gene', 'begin', 'end'])), "Required columns missing"
        trim_table_dict = dict([(v, (b, e)) for v, b, e in trim_table[['gene', 'begin', 'end']].values])

    if args.gene == 'V':
        all_trimmed = trim_Vs(db_df, end)
    elif args.gene == 'J':
        all_trimmed = trim_Js(db_df, begin)
    else:
        all_trimmed = trim_Ds(db_df, begin, end)

    # Fix trims if they're enforced by table
    for (tname, (tbegin, tend)) in trim_table_dict.items():
        logger.info(f"Overwriting with trim table {tname}")
        all_trimmed.loc[all_trimmed.gene == tname,'trims'] = f"{tbegin},{tend}"
        all_trimmed.loc[all_trimmed.gene == tname,'sequence_trimmed'] = [s[tbegin:len(s)-tend] for s in all_trimmed.loc[all_trimmed.gene == tname,'sequence'].values]

    # Extract relevant dictionaries
    db = dict(all_trimmed[['seqname', 'sequence_trimmed']].values)
    trimmed = dict(all_trimmed[['seqname', 'trims']].values)

    # Load sequences which are identified germline and should be used for counting
    vused = []
    if args.vfasta:
        vused = [record.name for record in SeqIO.parse(args.vfasta, 'fasta')]
    dused = []
    if args.dfasta:
        dused = [record.name for record in SeqIO.parse(args.dfasta, 'fasta')]
    jused = []
    if args.jfasta:
        jused = [record.name for record in SeqIO.parse(args.jfasta, 'fasta')]

    # Load filtered tab
    filtered = read_table(args.filtered, usecols = CORECOUNT_COLUMNS)
    filtered.np1 = filtered.np1.fillna('').astype(str)
    filtered.np2 = filtered.np2.fillna('').astype(str)
    filtered.D_region = filtered.D_region.fillna('').astype(str)

    # Conjugate column
    filtered = filtered.assign(VDJ_junction=filtered.np1.str.cat(filtered.D_region, na_rep='').str.cat(filtered.np2, na_rep=''))

    # Search
    matches = search(filtered, db, trimmed, dict(db_df.values), args.gene, args.segment, vused, dused, jused, v_anchor, j_anchor, args.lcs, args.lcs_ignore, args.lcs_coverage, verbose=args.verbose)

    frequency_only_passed = pd.DataFrame()
    if args.expected:        
        expected = pd.read_table(args.expected)
        assert set(expected.columns) >= set(['gene', 'frequency']), "Required columns missing"
        # whitelist in alleles on basis of frequency alone 
        if "frequency_only" in expected.columns:
            # handle missing values
            expected = expected.assign(frequency_only = expected["frequency_only"].apply(lambda x: str(x) == "True"))
            matches = matches.assign(count_frequency=(matches['count']/matches['count'].sum()))
            frequency_only = expected.query("frequency_only")
            frequency_only_passed = filter_by_expected(matches.query("name in @frequency_only.gene or gene in @frequency_only.gene"), frequency_only)

    # Assignment filters
    if args.strict_filtering:
        logger.info("Assignment filtering with V_errors<={}, J_errors<={}, V_coverage>={}, J_covereage>={} and no_stop codon.".format(args.v_errors, args.j_errors, args.v_covered*100, args.j_covered*100))
        matches = matches[(matches.V_errors <= args.v_errors) & (matches.J_errors <= args.j_errors) & (matches.stop_codon == "F") & (matches.V_covered >= (args.v_covered*100)) & (matches.J_covered >= (args.j_covered*100))]

    # Filtering
    if len(matches) > 0:
        matches = compute_frequencies(matches, args.allelic_group)
        if args.uout:
            matches.to_csv(args.uout, sep='\t', index=False, float_format='%.5f')
        matches_filtered = filter(matches, args.ratio, args.mincount, args.lenmaxfreq, args.seqmaxfreq)

        # Compute count frequency
        matches_filtered = matches_filtered.assign(count_frequency=(matches_filtered['count']/matches_filtered['count'].sum()))
        if args.expected:
            matches_filtered = filter_by_expected(matches_filtered, expected)
            if len(frequency_only_passed) > 0:
                names_to_add = list(set(frequency_only_passed["name"].values).difference(set(matches_filtered["name"].values)))
                matches_filtered = pd.concat([matches_filtered, frequency_only_passed[frequency_only_passed['name'].isin(names_to_add)]], ignore_index = True)
        # Save output
        matches_filtered.to_csv(args.output, sep='\t',index=False, float_format='%.5f')
    else:
        logger.warn("No matches, you must be doing something wrong!")
