"""
Select sequences from the same library according to the index identifier

In case when libraries are prepared with the use of index at 5' end or at 3' end,
sequences from the different ones can be distinguished by sucg indexes.
In that way lickage of sequences from non-target libraries can be caught
and filtered out.
The most frequent index is set as the initital library index. Check json file for
three most frequent indexes and their percent.
"""

import pandas as pd
import numpy as pd
from itertools import islice
import dnaio
import logging
from collections import defaultdict
import sys
import os
import json

logger = logging.getLogger(__name__)

def add_arguments(parser):
    arg = parser.add_argument
    arg('--json', metavar="FILE", help="Write statistics to FILE")
    arg('path_merged', help='Merged reads compressed FASTQ file')
    arg('index_len', help='Length of index')
    arg('output',  help='Output file (compressed FASTQ)')

def select_library_reads(fastx, index_length, output_file):
    """
    index -- at 5' end
    fastx -- path to merged FASTQ input
    """
    # Map indexes to lists of sequences
    index_length = int(index_length)
    indexes = defaultdict(list)
    n = 0

    with dnaio.open(fastx) as f:
        for record in islice(f, 0, None):
            if index_length > 0:
                index = record.sequence[:index_length]
                unindexed = record[index_length:]
            else:
                index = record.sequence[index_length:]
                unindexed = record[:index_length]

            indexes[index].append(unindexed)
            n += 1

    index_counts = {k:len(v) for k, v in indexes.items()}
    sorted_index_freq = sorted(index_counts, key=index_counts.get, reverse=True)
    most_freq_index = sorted_index_freq[0]

    if index_length > 0:
        logger.info("Index from 5' end was used")
    else:
        logger.info("Index from 3' end was used")
    logger.info('%s sequences before index selection', n)
    logger.info('%s sequences have the library index', index_counts[most_freq_index])
    logger.info('%s percent sequences have the defined index', round((index_counts[most_freq_index]/n)*100, 1))

    with dnaio.open(output_file, mode="w") as writer:
        for record in indexes[most_freq_index]:
            writer.write(record)

    return sorted_index_freq, index_counts

def main(args):
    indexes, counts = select_library_reads(fastx=args.path_merged,
                                        index_length=args.index_len,
                                        output_file=args.output)
    if args.json:
        stats = {
            indexes[0]: counts[indexes[0]],
            indexes[1]: counts[indexes[1]],
            indexes[2]: counts[indexes[2]]
        }
        with open(args.json, 'w') as f:
            json.dump(stats, f, indent=2)
            print(file=f)
