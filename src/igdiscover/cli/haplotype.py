"""
Determine haplotypes based on co-occurrences of alleles
"""
import sys
import logging
from typing import List, Tuple, Iterator
from itertools import product
from argparse import ArgumentParser
import pandas as pd
import dnaio
from ..table import read_table

logger = logging.getLogger(__name__)

# The second-most expressed allele of a gene must be expressed at at least this
# fraction of the highest-expressed allele in order for the gene to be considered
# heterozygous.
HETEROZYGOUS_THRESHOLD = {'v': 0.1, 'd': 0.2, 'j':0.1}

# Deletion if expressed below this ratio
EXPRESSED_RATIO = {'v': 0.05, 'd': 0.05, 'j': 0.001}


def add_arguments(parser: ArgumentParser):
    arg = parser.add_argument
    arg('--v-gene', action='append',
        help='V gene to use for haplotyping J. Default: Auto-detected')
    arg('--j-gene', action='append',
        help='J gene to use for haplotyping V. Default: Auto-detected')
    arg('--d-gene', action='append',
        help='D gene to use for haplotyping V. Default: Auto-detected')
    arg('--v-with', 
        default='j', 
        choices=('j', 'd'),
        help='Gene used for haplotyping Vs. Default: %(default)s')
    arg('--d-with',
        default='j',
        choices=('j', 'v'),
        help='Gene used for haplotyping Ds. Default: %(default)s')
    arg('--j-with',
        default='v',
        choices=('d', 'v'),
        help='Gene used for haplotyping Js. Default: %(default)s')
    arg('--vj-pool', 
        action='store_true',
        default=False,
        help='Pool Vs when haplotyping Js. Default: %(default)s')
    arg('--v-errors',
        type=int, default=1,
        help='Maximum allowed number of errors in Vs')
    arg('--j-errors',
        type=int,
        default=1,
        help='Maximum allowed number of errors in Js')
    arg('--d-evalue',
        type=float,
        default=1E-4,
        help='Maximal allowed E-value for D gene match. Default: %(default)s')
    arg('--d-coverage',
        '--D-coverage',
        type=float,
        default=50,
        help='Minimum D coverage (in percent). Default: %(default)s%%)')
    arg('--v-restrict', 
        metavar='FASTA',
        help='Restrict V analysis to the genes named in the FASTA file. '
            'Only the sequence names are used!')
    arg('--d-restrict',
        metavar='FASTA',
        help='Restrict D analysis to the genes named in the FASTA file. '
            'Only the sequence names are used!')
    arg('--j-restrict',
        metavar='FASTA',
        help='Restrict J analysis to the genes named in the FASTA file. '
            'Only the sequence names are used!')

    arg('--order',
        metavar='FASTA',
        default=None,
        help='Sort the output according to the order of the records in '
        'the given FASTA file.')
    arg('--plot',
        metavar='FILE',
        default=None,
        help='Write a haplotype plot to FILE')
    arg('--show-unknown',
        action='store_true',
        default=True,
        help='Whether to sahow genes with unknown haplotype')
    arg('--log-counts', 
        action='store_true',
        default=False,
        help='Log transform counts for plotting.')
    arg('--haplo',
        metavar='haplo',
        default=None,
        help='Write haplotype table in parsable TSV format')
    arg('--expressed-tsv', 
        metavar='expressed_tsv',
        default=None,
        help='Deletion if expressed below this ratio but in TSV format for gene families. Columns are (gene, ratio).')
    arg('--structure-plot',
        metavar='FILE',
        default=None,
        help=
        'Write a haplotype structure plot (counts binarized 0 and 1) to FILE')
    arg('table', help='Table with parsed and filtered IgBLAST results')


def expression_matrix(table, anchoring_gene='j', target_gene='v'):
    """
    Helper function computing co-expression matrix
    """
    gene1 = target_gene + '_call'
    gene2 = anchoring_gene + '_call'
    expression_counts = table.groupby([gene1, gene2]).size().to_frame().reset_index()
    matrix = pd.DataFrame(expression_counts.pivot(index=gene1, columns=gene2, values=0).fillna(0), dtype=int)
    return matrix

def initial_het(matrix, anchoring_pair, absmin=50, ratiomin=0.10):
    """
    Function to perform initial V anchoring with a known heterozygous allele pair.
    """
    # We care only about anchoring_pair now
    anchored_matrix = matrix[anchoring_pair]

    # Only alleles which have high enouch absolute counts
    anchored_counts = anchored_matrix[(anchored_matrix >= absmin).any(axis=1)]

    # Calcualte ratios per allele, both counts with respect to total sum
    ratios = anchored_counts.div(anchored_counts.sum(axis=1),axis=0)

    # Selection of obvious heterozygous Vs which have good hemizygous or heterozygous looking ratios per allele
    het_s = anchored_counts[(ratios.min(axis=1) < ratiomin) & (ratios.max(axis=1) > (1-ratiomin))]

    # Extract good Vs from either of anchors
    het_s_x = het_s.index[het_s.iloc[:,0]>het_s.iloc[:,1]]
    het_s_y = het_s.index[het_s.iloc[:,0]<=het_s.iloc[:,1]]
    return het_s_x, het_s_y

def pool_alleles(matrix, anchoring_pair, target_gene='V', absmin=50, ratiomin=0.10):
    """
    Function returns pooled alleles across target_gene
    """
    (x_alleles,y_alleles) = initial_het(matrix, anchoring_pair, absmin, ratiomin)
    x_pool = matrix[matrix.index.get_level_values(target_gene+'_call').isin(x_alleles)].sum(axis=0).to_frame(name='pool_x')
    y_pool = matrix[matrix.index.get_level_values(target_gene+'_call').isin(y_alleles)].sum(axis=0).to_frame(name='pool_y')
    return pd.concat([x_pool,y_pool],axis=1).rename({'pool_x':'a_pool','pool_y':'b_pool'},axis=1)


def expression_counts(table: pd.DataFrame,
                      gene_type: str) -> Iterator[pd.DataFrame]:
    """
    Yield DataFrames for each gene with gene and allele as the row index and columns 'name' and
    'count'. For example, when 'name' is VH1-1*01, gene would be 'VH1-1' and allele
    would be '01'.
    """
    counts = table.dropna().groupby(gene_type + '_call').size()
    names, _, alleles = zip(*[s.partition('*') for s in counts.index])
    expressions = pd.DataFrame(
        {
            'gene': names,
            'allele': alleles,
            'count': counts,
            'name': counts.index
        },
        columns=['gene', 'allele', 'name',
                 'count']).set_index(['gene', 'allele'])
    del alleles

    # Example expressions at this point:
    #
    #                             name  count
    # gene       allele
    # IGHV1-18   01        IGHV1-18*01    166
    #            03        IGHV1-18*03      1
    # IGHV1-2    02         IGHV1-2*02     85
    #            04         IGHV1-2*04     16
    # IGHV1-24   01        IGHV1-24*01      5

    logger.info('Heterozygous %s genes:', gene_type)
    for _, alleles in expressions.groupby(level='gene'):
        # Remove alleles that have too low expression relative to the highest-expressed allele
        max_expression = alleles['count'].max()
        alleles = alleles[alleles['count'] >= HETEROZYGOUS_THRESHOLD[gene_type] * max_expression]

        if len(alleles) >= 2:
            logger.info('%s with alleles %s -- Counts: %s',
                        alleles.index[0][0], ', '.join(alleles['name']),
                        ', '.join(str(x) for x in alleles['count']))
        yield alleles


class HeterozygousGene:

    def __init__(self, name: str, alleles: List[str]):
        """
        name -- name of this gene, such as 'VH4-4'
        alleles -- list of its alleles, such as ['VH4-4*01', 'VH4-4*02']
        """
        self.name = name
        self.alleles = alleles


def compute_coexpressions(table: pd.DataFrame, gene_type1: str,
                          gene_type2: str):
    assert gene_type1 != gene_type2
    coexpressions = table.groupby([gene_type1 + '_call', gene_type2 + '_call']).size().to_frame()
    coexpressions.columns = ['count']
    return coexpressions


def cooccurrences(coexpressions, het_alleles: Tuple[str, str], target_groups, gene_type, expressed_ratio):
    """
    het_alleles -- a pair of alleles of a heterozygous gene,
    such as ('IGHJ6*02', 'IGHJ6*03').
    """
    assert len(het_alleles) == 2

    haplotype = []
    for target_alleles in target_groups:
        is_expressed_list = []
        names = []
        counts = []
        for target_allele, _ in target_alleles.itertuples(index=False):
            ex = []
            for het_allele in het_alleles:
                try:
                    e = coexpressions.loc[(het_allele, target_allele), 'count']
                except KeyError:
                    e = 0
                ex.append(e)
            ex_total = sum(ex) + 1  # +1 avoids division by zero
            ratios = [x / ex_total for x in ex]
            target_gene = target_allele.split('*',1)[0]
            # Alter defaults if gene provided, otherwise always update with defaults
            EXPRESSED_RATIO[gene_type] = expressed_ratio.get(target_gene, EXPRESSED_RATIO[gene_type])

            is_expressed = [ratio >= EXPRESSED_RATIO[gene_type] for ratio in ratios]
            if is_expressed != [False, False]:
                is_expressed_list.append(is_expressed)
                names.append(target_allele)
                counts.append(ex)
        if len(is_expressed_list) == 1:
            is_expressed = is_expressed_list[0]
            if is_expressed == [True, False]:
                haplotype.append((names[0], '', 'deletion', counts[0]))
            elif is_expressed == [False, True]:
                haplotype.append(('', names[0], 'deletion', counts[0]))
            elif is_expressed == [True, True]:
                haplotype.append((names[0], names[0], 'homozygous', counts[0]))
            else:
                assert False
        elif is_expressed_list == [[True, False], [False, True]]:
            haplotype.append((names[0], names[1], 'heterozygous',
                              (counts[0][0], counts[1][1])))
        elif is_expressed_list == [[False, True], [True, False]]:
            haplotype.append((names[1], names[0], 'heterozygous',
                              (counts[0][1], counts[1][0])))
        else:
            type_ = 'unknown'
            # Somewhat arbitrary criteria for a "duplication":
            # 1) one heterozygous allele, 2) at least three alleles in total
            n_true = sum(x.count(True) for x in is_expressed_list)
            if ([True, False] in is_expressed_list
                    or [False, True] in is_expressed_list) and n_true > 2:
                type_ = 'duplication'
            # Additional rule, if more than on allele expressed on target chromosome, call it duplication
            unique_alleles = len(set(target_alleles))
            if ([True, True] in is_expressed_list) and (unique_alleles > 1):
                type_ = 'duplication'
            for is_expressed, name, count in zip(is_expressed_list, names,
                                                 counts):
                haplotype.append((
                    name if is_expressed[0] else '',
                    name if is_expressed[1] else '',
                    type_,
                    count,
                ))
    return haplotype


class HaplotypePair:
    """Haplotype pair for a single gene type (v/d/j)"""

    def __init__(self, haplotype, gene_type, het1, het2):
        self.haplotype = haplotype
        self.gene_type = gene_type
        self.het1 = het1
        self.het2 = het2

    def sort(self, order: List[str]) -> None:
        """
        Sort the haplotype
        order -- list a names of genes in the desired order
        """
        gene_order = {name: i for i, name in enumerate(order)}

        def keyfunc(hap):
            name = hap[0] if hap[0] else hap[1]
            gene, _, allele = name.partition('*')
            try:
                allele = int(allele)
            except ValueError:
                allele = 999
            try:
                index = gene_order[gene]
            except KeyError:
                logger.warning(
                    'Gene %s not found in gene order file, placing it at the end',
                    gene)
                index = 1000000
            return index * 1000 + allele

        self.haplotype = sorted(self.haplotype, key=keyfunc)

    def switch(self):
        """Swap the two haplotypes"""
        self.het2, self.het1 = self.het1, self.het2
        haplotype = []
        for name1, name2, type_, counts in self.haplotype:
            assert len(counts) == 2
            counts = counts[1], counts[0]
            haplotype.append((name2, name1, type_, counts))
        self.haplotype = haplotype

    def to_tsv(self, header: bool = True) -> str:
        lines = []
        if header:
            lines.append('\t'.join(
                ['haplotype1', 'haplotype2', 'type', 'count1', 'count2']))
        lines.append('# {} haplotype from {} and {}'.format(
            self.gene_type, self.het1, self.het2))
        for h1, h2, typ, count in self.haplotype:
            lines.append('\t'.join([h1, h2, typ,
                                    str(count[0]),
                                    str(count[1])]))
        return '\n'.join(lines) + '\n'


def plot_haplotypes(blocks: List[HaplotypePair],
                    show_unknown: bool = False,
                    binarize: bool = False,
                    log_counts: bool=False):
    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure
    from matplotlib.patches import Patch
    
    colormap = dict(homozygous='cornflowerblue', heterozygous='lightgreen', deletion='crimson',
        duplication='gold', unknown='gray')

    if not show_unknown:
        del colormap['unknown']
    labels = [[], []]
    heights = [[], []]
    names = []
    colors = []
    positions = []
    numbers = []
    pos = 0

    for b, block in enumerate(blocks):
        for hap1, hap2, type_, (count1, count2) in block.haplotype:
            if not show_unknown and type_ == 'unknown':
                continue
            if binarize:
                count1 = 1 if hap1 else 0
                count2 = 1 if hap2 else 0
            numbers.append(count1)
            numbers.append(count2)
            label = hap1 if hap1 else hap2
            hap1 = ('*' + hap1.partition('*')[2]) if hap1 else ''
            hap2 = ('*' + hap2.partition('*')[2]) if hap2 else ''
            if log_counts:
                # Use pseudo log counts because of zeros for visuals
                heights[0].append(np.log(count1+1))
                heights[1].append(np.log(count2+1))
            else:
                heights[0].append(count1)
                heights[1].append(count2)

            labels[0].append(hap1)
            labels[1].append(hap2)
            names.append(label.partition('*')[0])
            colors.append(colormap[type_])
            positions.append(pos)
            pos += 1
        pos += 2
    
    n = len(names)
    assert len(labels[0]) == len(labels[1]) == len(colors) == len(
        heights[0]) == len(heights[1]) == n

    fig = Figure(figsize=(8, 28))
    FigureCanvas(fig)
    axes = fig.subplots(ncols=2)
    for i in 0, 1:
        axes[i].barh(y=positions, width=heights[i], color=colors)
        axes[i].set_yticklabels(labels[i])
        axes[i].set_ylim((-0.5, max(positions) + 0.5))

    # Add center axis
    ax_center = axes[0].twinx()
    ax_center.set_yticks(axes[0].get_yticks())
    ax_center.set_ylim(axes[0].get_ylim())
    ax_center.set_yticklabels(names)

    # Synchronize x limits on both axes (has no effect if binarize is True)
    max_x = np.max(numbers)
    for ax in axes:
        ax.set_xlim(right=max_x)
    axes[0].invert_xaxis()

    for ax in axes[0], axes[1], ax_center:
        ax.invert_yaxis()
        for spine in ax.spines.values():
            spine.set_visible(False)
        if binarize:
            ax.set_xticks([])
        else:
            ax.set_axisbelow(True)
            ax.grid(True, axis='x')
        ax.set_yticks(positions)
        ax.tick_params(left=False, right=False, labelsize=12)
    axes[1].tick_params(labelleft=False, labelright=True)
    if not binarize:
        axes[0].spines['right'].set_visible(True)
        axes[1].spines['left'].set_visible(True)

    # Legend
    legend_patches = [
        Patch(color=col, label=label) for label, col in colormap.items()
    ]
    fig.legend(handles=legend_patches,
               loc='upper center',
               ncol=len(legend_patches))
    fig.tight_layout()
    fig.subplots_adjust(top=1 - 2 / len(names))

    return fig


def read_and_filter(path: str, d_evalue: float, d_coverage: float, verrors: int, jerrors: int):
    usecols = [
        'v_call', 'd_call', 'j_call', 'V_errors', 'D_errors', 'J_errors',
        'D_covered', 'd_support'
    ]
    # Support reading a table without D_errors
    try:
        table = read_table(path, usecols=usecols)
    except ValueError:
        usecols.remove('D_errors')
        table = read_table(path, usecols=usecols)
    logger.info('Table with %s rows read', len(table))

    table = table[table.V_errors <= verrors]
    logger.info('%s rows remain after requiring V errors <= %d', len(table),verrors)
    table = table[table.J_errors <= jerrors]
    logger.info('%s rows remain after requiring J errors <= %d', len(table),jerrors)
    table = table[table.d_support <= d_evalue]
    logger.info('%s rows remain after requiring D E-value <= %s', len(table),
                d_evalue)
    table = table[table.D_covered >= d_coverage]
    logger.info('%s rows remain after requiring D coverage >= %s', len(table),
                d_coverage)
    if 'D_errors' in table.columns:
        table = table[table.D_errors == 0]
        logger.info('%s rows remain after requiring D errors = 0', len(table))
    return table

def restrict(table, fasta, gene):
    """
    Helper function filtering table given names in FASTA
    """
    with SequenceReader(fasta) as sr:
        restrict_names = set(r.name for r in sr)
    table = table[table[gene].map(lambda name: name in restrict_names)]
    logger.info('After restricting to %r named in %r, %d rows remain', gene, fasta,
        len(table))
    if len(table) == 0:
        logger.error('No rows remain, cannot continue')
        sys.exit(1)
    return table


def main(args):
    expressed_ratio = {}
    if args.expressed_tsv:
        expressed_ratio = dict(read_table(args.expressed_tsv).values)
        logger.info(expressed_ratio)

    if args.order is not None:
        with dnaio.open(args.order) as sr:
            gene_order = [r.name for r in sr]
    else:
        gene_order = None

    table = read_and_filter(args.table, args.d_evalue, args.d_coverage, args.v_errors, args.j_errors)

    # Pre-filter based on known germline to exclude alles that are miss-assigned
    # Do not remove any alleles relative to the highest-expressed allele if alleles restricted
    if args.v_restrict is not None:
        table = restrict(table, args.v_restrict, 'v_call')
        HETEROZYGOUS_THRESHOLD['v'] = 0
    if args.d_restrict is not None:
        table = restrict(table, args.d_restrict, 'd_call')
        # We know what is expressed, disable checks
        HETEROZYGOUS_THRESHOLD['d'] = 0
    if args.j_restrict is not None:
        table = restrict(table, args.j_restrict, 'j_call')
        HETEROZYGOUS_THRESHOLD['j'] = 0


    expressions = dict()
    het_expressions = dict(
    )  # these are also sorted, most highly expressed first
    for gene_type in 'vdj':
        ex = list(expression_counts(table, gene_type))
        expressions[gene_type] = ex
        het_ex = [e for e in ex if len(e) == 2]
        if het_ex:
            # Pick most highly expressed
            het_expressions[gene_type] = sorted(het_ex,
                                                key=lambda e: e['count'].sum(),
                                                reverse=True)
        else:
            # Force at least something to be plotted
            het_expressions[gene_type] = [None]

    if args.v_gene:
        het_ex = [e for e in expressions['v'] if len(e) == 2]
        v_found = False
        for v_gene in args.v_gene:
            for ex in het_ex:
                if (args.v_gene in ex.index and not ex.loc[args.v_gene].empty) or (args.v_gene in ex['name'].values):
                    het_expressions['v'] = [ex]
                    v_found = True
                    break
            if v_found:
                break

        if not v_found:
            logger.error(
                'Neither of gene or allele %s was found in the list of heterozygous V genes. '
                'It cannot be used with the --v-gene option.',
                ','.join(args.v_gene))
            sys.exit(1)
    if args.d_gene:
        het_ex = [e for e in expressions['D'] if len(e) == 2]
        d_found = False
        for d_gene in args.d_gene:
            for ex in het_ex:
                if (d_gene in ex.index and not ex.loc[d_gene].empty) or (args.d_gene in ex['name'].values):
                    het_expressions['d'] = [ex]
                    d_found = True
                    break
            if d_found:
                break
        if not d_found:
            logger.error('Neither of gene or allele %s was found in the list of heterozygous D genes. '
                         'These cannot be used with the --d-gene option.', ','.join(args.d_gene))
            sys.exit(1)

    if args.j_gene:
        het_ex = [e for e in expressions['j'] if len(e) == 2]
        j_found = False
        for j_gene in args.j_gene:
            for ex in het_ex:
                if (j_gene in ex.index and not ex.loc[j_gene].empty) or (
                        args.j_gene in ex['name'].values):
                    het_expressions['j'] = [ex]
                    j_found = True
                    break
            if j_found:
                break
        if not j_found:
            logger.error(
                'Neither of gene or allele %s was found in the list of heterozygous J genes. '
                'These cannot be used with the --j-gene option.',
                ','.join(args.j_gene))
            sys.exit(1)

    block_lists = []

    # We want to avoid using a gene classifed as 'duplicate' for haplotyping, but the
    # classification is only known after we have done the haplotyping, so we try it
    # until we found a combination that works
    products = list(product(het_expressions['j'], het_expressions['v']))
    for attempt, (het_j, het_v) in enumerate(products):
        best_het_genes = {
            'v': het_v,
            'd': het_expressions['d'][0] if het_expressions['d'] else None,
            'j': het_j,
        }
        for gene_type in 'vdj':
            bhg = best_het_genes[gene_type]
            text = bhg.index[0][0] if bhg is not None else 'none found'
            logger.info('Heterozygous %s gene to use for haplotyping: %s',
                        gene_type, text)

        # Create HaplotypePair objects ('blocks') for each gene type
        blocks = []

        for target_gene_type, het_gene in (
            ('j', args.j_with),
            ('d', args.d_with),
            ('v', args.v_with),
        ):
            het_alleles = best_het_genes[het_gene]
            if het_alleles is None:
                continue
            logger.info("Haplotyping {} with {}.".format(target_gene_type, het_gene))
            coexpressions = compute_coexpressions(table, het_gene,target_gene_type)
            target_groups = expressions[target_gene_type]
            het1, het2 = het_alleles['name']

            # When J haplotyped with Vs use pooled counts instead
            if args.vj_pool and (target_gene_type == 'j') and (het_gene == 'v'):
                matrix = expression_matrix(table, anchoring_gene=target_gene_type, target_gene=het_gene)
                if best_het_genes.get('j', None) is None or len(best_het_genes.get('j', None)) == 0:
                    continue
                het_alleles_j = best_het_genes['j'].name.values
                pool = pool_alleles(matrix, anchoring_pair=het_alleles_j, target_gene='v', absmin=50, ratiomin=0.10)
                het1, het2 = ['a_pool', 'b_pool']
                # Convert pool to coexpression table format expected by the rest of the code
                coexpressions = pool.reset_index().melt(id_vars=['j_call']).rename({'variable':'v_call','value':'count'},axis=1).set_index(['v_call','j_call'])

            haplotype = cooccurrences(coexpressions, [het1, het2], target_groups, target_gene_type, expressed_ratio)
            block = HaplotypePair(haplotype, target_gene_type, het1, het2)
            if gene_order:
                block.sort(gene_order)
            blocks.append(block)

        if het_j is None or het_v is None:
            break
        het_used = set(
            sum([
                list(h['name'])
                for h in best_het_genes.values() if h is not None
            ], []))
        het_is_duplicate = False
        for block in blocks:
            if block.gene_type == 'd':
                continue

            # This nested loop is put in a separate generator so we can easily 'break'
            # out of both loops at the same time.
            def nameiter():
                for name1, name2, type_, _ in block.haplotype:
                    for name in name1, name2:
                        yield name, type_

            for name, type_ in nameiter():
                if name in het_used and type_ != 'heterozygous':
                    het_is_duplicate = True
                    if not args.v_gene:
                        logger.warning(
                            '%s not classified as "heterozygous" during haplotyping, '
                            'attempting to use different alleles', name)
                    break
            if het_is_duplicate:
                break
        block_lists.append(blocks)
        if not het_is_duplicate:
            break
    else:
        if not args.v_gene:
            logger.warning(
                'No other alleles remain, using first found solution')
        blocks = block_lists[0]

    # Get the phasing right across blocks (i.e., swap J haplotypes if necessary)
    # assert len(blocks) in (0, 1, 3)
    if len(blocks) == 3:
        j_hap, d_hap, v_hap = blocks
        assert j_hap.gene_type == 'j'
        assert d_hap.gene_type == 'd'
        assert v_hap.gene_type == 'v'
        #assert d_hap.het1 == v_hap.het1
        #assert d_hap.het2 == v_hap.het2
        if d_hap.het1 != v_hap.het1:
            logger.warning("Haplotyping blocks with different heterozygous alleles {}".format([d_hap.het1, v_hap.het1]))
        if d_hap.het2 != v_hap.het2:
            logger.warning("Haplotyping blocks with different heterozygous alleles {}".format([d_hap.het2, v_hap.het2]))

        for name1, name2, _, _ in j_hap.haplotype:
            if (name1, name2) == (v_hap.het2, v_hap.het1):
                j_hap.switch()
                break
    else:
        # Incomplete haplotype we forgot to provide something
        logger.warning("Either V, D or Js cannot be haplotyped, but full haplotype is required.".format(len(blocks)))

    # Print the table
    header = True
    if args.haplo:
        block_list = [b.to_tsv(header=header) for b in blocks]
        with open(args.haplo, 'wt') as fout:
            fout.write('\n'.join(block_list))
    for block in blocks:
        print(block.to_tsv(header=header))
        header = False

    if (args.plot or args.structure_plot) and len(blocks) == 0:
        logger.error("No blocks to plot!")
    # Create plots if requested
    elif args.plot:
        fig = plot_haplotypes(blocks, show_unknown=args.show_unknown, log_counts=args.log_counts)
        fig.savefig(args.plot)
    elif args.structure_plot:
        fig = plot_haplotypes(blocks, binarize=True)
        fig.savefig(args.structure_plot)
