"""
Extract sequences from genomic data using regex for RSS pattern and known alleles database.
"""
import sys
import logging
import pandas as pd
from Bio import SeqIO
from Bio import Align
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import regex as re
import gzip
from tqdm import tqdm

tqdm.pandas() # Register progress bar for pandas

logger = logging.getLogger(__name__)

def add_arguments(parser):
	arg = parser.add_argument
	arg("assigned", help='Assigned table from IgDiscover.')
	arg("database", help='Coma separated list of databases.')
	arg("genomic", help='Table to store database.')
	arg('-e','--max-errors',type=int, default=10, help='Maximum number of allowed errors (10)')
	arg('-c','--min-coverage',type=int, default=90, help='Minimum coverage (90)')
	arg('-f','--max-heptamer-errors',type=int, default=1, help='Minimum heptamer errors (1)')
	arg('-l','--max-nonamer-errors', type=int, default=4, help='Minimum nonamer errors (4)')
	arg('-o','--min-occurences', type=int, default=15, help='Minimum number of occurences (15)')
	arg('-m','--min-occurences-final',type=int, default=3, help="Minimum number of occurences after demultiplexing (3).")
	arg('-x','--exclude', help='Tab separated table with genes to exclude (gene, sequence).')
	arg('-b','--barcodes',help='Barcodes to demultiplex to wells.')
	arg('-r','--reads',help='Reads in gzip compressed fasta file.')
	arg('-a','--allelic-ratio',type=float,default=0.2, help='Use allelic ratio filter to exclude false positives (0.2).')

def load_filtered(path, min_coverage=90, max_errors=10):
    assignments = pd.read_csv(path,sep='\t',usecols=['count','v_call','V_covered','V_errors','V_nt','genomic_sequence'])
    logger.info("Loaded {} sequences from assigned.tab".format(assignments.shape[0]))
    assignments = assignments.dropna()
    logger.info("NA droping {}".format(assignments.shape[0]))
    filtered = assignments[(assignments.V_covered > min_coverage) & (assignments.V_errors < max_errors)]
    logger.info("Filtered {} sequences after coverage and max errors thresholds".format(filtered.shape[0]))
    return filtered

IGHV_heptamer = [
    'CACACGC',
    'CACAAAG',
    'CACAATG',
    'CACAGAG',
    'CACAGTG'
    ]
IGHV_nonamer = [
    'ACACAAACG',
    'ACAAGAACC',
    'TCTAAAACC',
    'GCAGAAACC',
    'TCAGAAACC',
    'ACACAAACC',
    'ACAAAAACC',
    'TCAGAAACG'
    ]

def match_site(allele_seq, genomic_seq, heptamer_rule, nonamer_rule):
    r = re.compile('.*'+allele_seq+'([ACTC]*)'+heptamer_rule+nonamer_rule)
    m = r.match(genomic_seq)
    if m:
        return m.groups()
    return ['','','','']

class Merger:
    def __init__(self):
        self._items = []

    def add(self, item):
        items = []
        for existing_item in self._items:
            m = self.merged(existing_item, item)
            if m is None:
                items.append(existing_item)
            else:
                item = m
        items.append(item)
        self._items = items

    def __iter__(self):
        yield from self._items

    def __len__(self):
        return len(self._items)

    def merged(self, existing_item, item):
        if (existing_item in item) | (item in existing_item):
            return existing_item
        return None

def load_reads(fastagz):
    records = {}
    with gzip.open(fastagz,'rt') as fh:
        for record in SeqIO.parse(fh, 'fasta'):
            records.setdefault('read',[]).append(record.name)
            records.setdefault('genomic_sequence',[]).append(str(record.seq))
    return pd.DataFrame.from_dict(records)

def load_barcodes(barcode_path):
    return list([(i,(f,r)) for i,(f,r) in enumerate(pd.read_csv(barcode_path,sep='\t',usecols=['Forward_index','Reverse_index']).values,1)])

#def assign_well(seq, barcodes):
#    for well,(forward,reverse) in barcodes:
#        if seq.startswith(forward) & seq.endswith(reverse):
#            return well
#    return None
# Instead of assign_well we use find_well

def find_forward(seq, barcodes):
    for well,(forward,reverse) in barcodes:
        pos = seq.find(forward)
        if pos >= 0:
            return well,pos
    return None,None

def find_reverse(seq, barcodes):
    for well,(forward,reverse) in barcodes:
        pos = seq.find(reverse)
        if pos >= 0:
            return well,pos
    return None,None

def hamming_distance(seq1, seq2):
    return sum(c1 != c2 for c1, c2 in zip(seq1, seq2))

def reverse_primer_distance(seq, primer):
    return hamming_distance(seq[len(seq)-len(primer):len(seq)],primer)

def forward_primer_distance(seq, primer):
    return hamming_distance(seq[0:len(primer)],primer)

def find_well(seq, barcodes, dist=1, trim=20):
    forward = find_forward(seq,barcodes)
    reverse = find_reverse(seq,barcodes)
    if any(forward) & (not any(reverse)):
        reverse_barcode = barcodes[forward[0]-1][1][1]
        if any([i+1 for i in range(0,trim) if reverse_primer_distance(seq[0:len(seq)-i],reverse_barcode) <= dist]):
            return forward[0]
    elif (not any(forward)) & any(reverse):
        forward_barcode = barcodes[reverse[0]-1][1][0]
        if any([i+1 for i in range(0,trim) if forward_primer_distance(seq[i:len(seq)],forward_barcode) <= dist]):
            return reverse[0]
    elif any(forward) & any(reverse):
        return forward[0]
    return None

def assign_target(seq, target_db):
    for target_name, target_sequence in target_db.values:
        if target_sequence in seq:
            return target_name
    return None

def main(args):
	db = []
	for file in args.database.split(','):
		db += [(r.name,str(r.seq)) for r in SeqIO.parse(file, 'fasta')]
	filtered = load_filtered(args.assigned,min_coverage=args.min_coverage, max_errors=args.max_errors)

	#IGHV_heptamer_rule = '('+'|'.join(IGHV_heptamer)+'){s<='+str(args.max_heptamer_errors)+'}([ATGC]{23})'
	# New heptamer rule, such that first 3nts are conserved
	IGHV_heptamer_rule = '('+','.join(set([h[0:3] for h in IGHV_heptamer]))+'('+'|'.join(set([h[3:] for h in IGHV_heptamer]))+'){s<='+str(args.	max_heptamer_errors)+'})([ATGC]{23})'

	IGHV_nonamer_rule = '('+'|'.join(IGHV_nonamer)+'){s<='+str(args.max_nonamer_errors)+'}'

	# Produce candidates
	logger.info("Extract sequences up to RSS...")
	candidates = []
	for v_call, group in tqdm(filtered.groupby('v_call')):
		# Find RSS
		rss = group.apply(lambda x: (match_site(x['V_nt'],x['genomic_sequence'], IGHV_heptamer_rule, IGHV_nonamer_rule)), axis=1)
		end = [x[0] for x in rss]
		heptamer = [x[1] for x in rss]
		spacer = [x[2] for x in rss]
		nonamer = [x[3] for x in rss]

    	# Add rss
		group = group.assign(end=end,heptamer=heptamer,spacer=spacer,nonamer=nonamer)

    	# Combine V_nt with spacer before heptamr
		group = group.assign(candidate_sequence=group.V_nt + group.end)

    	# Keep only sequences with valid heptamer
		valid = group[(group.heptamer.str.len() == 7)][['count','v_call','candidate_sequence','heptamer','spacer','nonamer','genomic_sequence']]

    	# Valid
		valid = valid.assign(candidate_length=valid.candidate_sequence.str.len())

    	# Save
		candidates.append(valid)
	candidate_table = pd.concat(candidates)
	candidate_table.to_csv('candidate_table.tab',sep='\t',index=False)
	logger.info('Found RSS for {} sequences'.format(candidate_table.shape[0]))

	candidate_counts = candidate_table.groupby('candidate_sequence').size().reset_index(name='collapsed_count')
	candidate_rawcounts = candidate_table.groupby('candidate_sequence')['count'].sum().reset_index(name='uncollapsed_count')

	# Add counts to candidate
	candidate_table = pd.merge(candidate_table.drop('count',axis=1),candidate_counts, on='candidate_sequence',how='left')
	candidates_table = pd.merge(candidate_table,candidate_rawcounts, on='candidate_sequence',how='left')

	# Drop duplicats
	unique_table = candidates_table.drop_duplicates('candidate_sequence')

	# Filter by occurences
	filtered_table = unique_table[unique_table.collapsed_count >= args.min_occurences]

	logger.info('Kept {} sequences with minimum number ({}) of exact occurences.'.format(filtered_table.shape[0], args.min_occurences))

	logger.info("Merge overlapping sequences.")
	m = Merger()
	for s in sorted(filtered_table.candidate_sequence, key=lambda x: len(x),reverse=True):
		m.add(s)
	passed = set([s for s in m])
	passed_df = filtered_table[filtered_table.candidate_sequence.isin(passed)]
	logger.info('Kept non-overlapping unique {} sequences'.format(passed_df.shape[0]))


	logger.info('Name candidates.')
	renamed = pd.concat([group.assign(name=['{}_N{}'.format(n,i) for i,n in enumerate(group.v_call,1)]) for gene, group in passed_df.groupby('v_call')])

	# If external database provided rename known to that
	if len(db) > 0:
		renamed_db = []
		for _,row in tqdm(renamed.iterrows()):
			for db_name, db_seq in db:
				if (db_seq in row['candidate_sequence']) or (row['candidate_sequence'] in db_seq):
					row['name'] = db_name
			renamed_db.append(row)
		renamed = pd.DataFrame.from_records(renamed_db)

	# If psuedo-genes should be excluded
	if args.exclude:
		logger.info("Exclude pseudo genes using provided list.")
		exclude = pd.read_csv(args.exclude, sep='\t')
		renamed = renamed.assign(gene=renamed.name.apply(lambda x: x.split('*')[0]))
		tmp_table = pd.merge(renamed,exclude,on='gene',how='left').fillna('')
		index = tmp_table.apply(lambda x: (x['sequence'] in x['candidate_sequence']) & (len(x['sequence']) > 0),axis=1)
		renamed = tmp_table[~index].drop('sequence',axis=1)

	if args.barcodes and args.reads:
		barcodes = load_barcodes(args.barcodes)
		reads = load_reads(args.reads)
		target_db = renamed[['name','candidate_sequence']]

		# Assign wells
		logger.info("Demultiplex reads.")
		demuxed = reads.assign(well=reads.genomic_sequence.progress_apply(lambda x: find_well(x, barcodes, 1, 5)))
		demuxed_accepted = demuxed[~demuxed.well.isna()]

		# Assign targets
		logger.info("Assigned new database.")
		demuxed_targets = demuxed_accepted.assign(target_name=demuxed_accepted.genomic_sequence.progress_apply(lambda x: assign_target(x, target_db))).fillna('')

		# Count targets
		demuxed_counts = demuxed_targets.groupby(['well','target_name']).size().reset_index(name='occurences')
		# Assign gene
		demuxed_counts = demuxed_counts.assign(gene=demuxed_counts.target_name.apply(lambda x: x.split('*')[0]))

		# Calculate ratios
		logger.info("Calculate allelic ratios.")
		ratios = pd.concat([group.assign(ratio=group['occurences']/group['occurences'].sum()) for (well,gene),group in demuxed_counts.groupby(['well','gene'])])

		# Fix column types and add back columns discarded during dataframe manipulation
		ratios = ratios.assign(well=ratios.well.astype(int))
		ratios = pd.merge(ratios,renamed[['name','candidate_sequence']],left_on=['target_name'],right_on=['name'],how='left')

		# Filter ratios
		out = ratios[(ratios.ratio >= args.allelic_ratio) &(ratios.occurences >= args.min_occurences_final) & (ratios.target_name.str.len() > 0)]

		# Save results
		ratios.to_csv(args.genomic.split('.')[0]+'_candidates.tsv', sep='\t', index=False)
		out.to_csv(args.genomic.split('.')[0]+'_final.tsv', sep='\t', index=False)

	renamed.to_csv(args.genomic, sep='\t', index=False)
	logger.info("Output with {} unique sequences saved to {}".format(len(renamed), args.genomic))
