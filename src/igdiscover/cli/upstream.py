"""
Cluster upstream sequences (UTR and leader) for each gene

For each gene, look at the sequences assigned to it. Take the upstream
sequences and compute a consensus for them. Only those assigned sequences are
taken that have a very low error rate for the V gene match.

Output a FASTA file that contains one consensus sequence for each gene.
"""
import logging
from collections import Counter
import re
import pandas as pd

from ..table import read_table
from ..align import iterative_consensus
from Bio import SeqIO

logger = logging.getLogger(__name__)

# When computing a UTR consensus, ignore sequences that deviate by more than
# this factor from the median length.
UTR_MEDIAN_DEVIATION = 0.1

UPSTREAM_COLUMNS = ["v_call", "d_call", "j_call", "V_SHM", "J_SHM", "leader", "sequence"]

def add_arguments(parser):
    arg = parser.add_argument
    arg('--max-V-errors', '--max-error-percentage', '-e', dest='max_v_errors',
        metavar='PERCENT', type=float, default=1,
        help='Allow PERCENT errors in V gene match. Default: %(default)s')
    arg('--max-FR1-errors', dest='max_fr1_errors', metavar='PERCENT', type=float, default=None,
        help='Allow PERCENT errors in FR1 region.')
    arg('--max-CDR1-errors', dest='max_cdr1_errors', metavar='PERCENT', type=float, default=None,
        help='Allow PERCENT errors in CDR1 region.')
    arg('--min-consensus-size', type=int, default=1, metavar='N',
        help='Require at least N sequences for consensus. Default: %(default)s')
    arg('--consensus-threshold', '-t', metavar='PERCENT',
        type=float, default=75,
        help='Threshold for consensus computation. Default: %(default)s%%')
    arg('--no-ambiguous', '--no-N', default=False, action='store_true',
        help='Discard consensus sequences with ambiguous bases')
    arg('--split-ambiguous', default=False, action='store_true',
        help='Try splitting upstream variants with Ns.')
    arg('--variant-minfreq',default=0.15, type=float, help='Minimum frequency of ambiguous variants to retain (default 0.15).')
    arg('--variant-minsize',default=10, type=int, help='Minimum number of sequences to consider for variant splitting (default 10).')
    arg('--part', choices=['UTR', 'leader', 'UTR+leader'],
            default='leader', help='Which part of the sequence before the V '
            'gene match to analyze. Default: %(default)s')
    arg('--db', help='Restrict leaders only to sepcific set of sequences in FASTA')
    arg('--log', default=False, action='store_true', help='Enable debugging output')
    arg('--keep', default=True, action='store_false',
        help='Disable regex trimming that assures that sequences start at ATG (default: enabled).')
    arg('--anchor', type=str,
        help='Coma separated pair of two heterozygous alleles to use for counting for each reported sequence')
    arg('--outtab', help='Table to store upstream summary')
    arg('table', help='Table with parsed IgBLAST results (assigned.tab.gz or '
        'filtered.tab.gz)')


def main(args):
    keep = re.compile('.*(ATG[ATGC]{53-63})$')
    pair = None
    if args.anchor:
        logger.info('Counting with user provided %s', args.anchor)
        pair = args.anchor.split(',')
        assert len(pair) == 2, "At most two alleles can be used for counting."
    assert not ((args.keep) & ('UTR' in args.part)), 'Regex can only be used with UTR part, or disable regex (--keep option).'
    
    if args.log:
        logging.getLogger().setLevel(logging.DEBUG)
    table = read_table(args.table, usecols = UPSTREAM_COLUMNS)
    n_genes = len(set(table['v_call']))
    logger.info('%s rows read with %d unique gene names', len(table), n_genes)
    for name, column_name, arg in (
            ('V%SHM', 'V_SHM', args.max_v_errors),
            ('FR1%SHM', 'FR1_SHM', args.max_fr1_errors),
            ('CDR1%SHM', 'CDR1_SHM', args.max_cdr1_errors)):
        if arg is not None:
            table = table[getattr(table, column_name) <= arg]
            logger.info('%s rows remain after discarding %s > %s%%',
                len(table), name, arg)
    table['UTR+leader'] = table['UTR'] + table['leader']
    table = table[table[args.part] != '']
    table['UTR_length'] = [ len(s) for s in table['UTR'] ]
    # Filter with external database
    if args.db:
        db = dict([(r.name, str(r.seq)) for r in SeqIO.parse(args.db, 'fasta')])
        ntotal = len(table)
        table = table[table['v_call'].isin(db.keys())]
        nfiltered = len(table)
        logger.info("%d rows out of %d discarded after DB filtering", ntotal-nfiltered, ntotal)

    n_written = 0
    n_consensus_with_n = 0
    out_table = {}
    for name, group in table.groupby('v_call'):
        assert len(group) != 0
        if len(group) < args.min_consensus_size:
            logger.info('Gene %s has too few assignments (%s), skipping.', name, len(group))
            continue

        counter = Counter(group['UTR_length'])
        j_counter = Counter(group['j_call'])
        logger.debug('Sequence length/count table: %s',
            ', '.join('{}: {}'.format(l, c) for l, c in counter.most_common()))

        if args.part == 'leader':
            sequences = list(group['leader'])
        else:
            # Since UTR lengths vary a lot, take all sequences that are at
            # least as long as the tenth longest sequence, and compute
            # consensus only from those.
            length_threshold = sorted(group['UTR_length'], reverse=True)[:10][-1]
            sequences = list(group[group['UTR_length'] >= length_threshold][args.part])

        if len(sequences) == 0:
            logger.info('Gene %s has %s assignments, but lengths are too different, skipping.', name, len(group))
            continue
        assert len(sequences) > 0
        if len(sequences) == 1:
            cons = sequences[0]
        else:
            # Remove region prior to ATG before building consensus if requested
            if args.keep:
                tmp = []
                for s in sequences:
                    m = keep.match(s)
                    if m:
                        tmp.append(m.group(1))
                if len(tmp) > 0:
                    sequences = tmp

            # Keep only those sequences whose length is at least 90% of the longest one
            cons = iterative_consensus(sequences, program='muscle-medium', threshold=args.consensus_threshold/100)
            # If the sequence lengths are different, the beginning can be unclear
            cons = cons.lstrip('N')
            logger.info('Gene %s has %s assignments, %s usable (%s unique sequences). '
            'Consensus has %s N bases.', name, len(group), len(sequences),
            len(set(sequences)), cons.count('N'))
        
        # Remove the region before ATG
        m = keep.match(cons)
        if m and args.keep:
            cons = m.group(1)

        ncount = cons.count('N')
        if  ncount > 0:
            n_consensus_with_n += 1
            if args.no_ambiguous:
                continue
        n_written += 1
        

        # Handle Ns
        if ('N' in cons) & args.split_ambiguous & (len(sequences) >= args.variant_minsize):
            logger.info("Found N bases in the %s consensus. Evaluating most frequent leader sequences to resolve this.", name)
            df = pd.DataFrame(sequences, columns=['sequences'])
            unique_seq = df.groupby('sequences').size().to_frame(name='count')
            unique_seq = unique_seq.assign(frequency=unique_seq['count'] / unique_seq['count'].sum())
            top = unique_seq[unique_seq.frequency > args.variant_minfreq].reset_index()
            for i,(seq,group) in enumerate(df[df.sequences.isin(top.sequences)].groupby('sequences'),1):
                ncount = seq.count('N')
                variant_name = name+'_'+str(i)+'_'+str(len(group))+'_N'+str(ncount)
                logger.info("Adding frequent leader variant %s instead of ambiguous consensus.",variant_name)
                out_table.setdefault('name', []).append(variant_name)
                out_table.setdefault('N_count',[]).append(ncount)
                out_table.setdefault('consensus', []).append(seq)
        else:
            # Add data to table
            out_table.setdefault('name', []).append(name)
            out_table.setdefault('N_count',[]).append(ncount)
            out_table.setdefault('consensus', []).append(cons)

        if pair:
            for j in pair:
                out_table.setdefault(j, []).append(dict(j_counter).get(j, 0))

    out_df = pd.DataFrame.from_dict(out_table)
    # Print out to fasta
    for i, row in out_df.iterrows():
        print('>{} consensus_{}\n{}'.format(row['name'], args.part, row['consensus']))

    if args.outtab:
        pd.DataFrame.from_dict(out_table).to_csv(args.outtab, sep='\t', index = False)

    in_or_ex = 'excluding' if args.no_ambiguous else 'including'
    logger.info('Wrote a consensus for %s of %s genes (%s %s with ambiguous bases)', n_written, n_genes, in_or_ex, n_consensus_with_n)