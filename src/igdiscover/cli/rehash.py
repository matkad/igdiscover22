"""
Rehash adds new _S hash values for all sequences in the FASTA file
"""
import sys
import logging
from Bio import SeqIO
from ..utils import unique_name

logger = logging.getLogger(__name__)

def add_arguments(parser):
    arg = parser.add_argument
    arg('input_fasta',metavar='INPUT_FASTA', help='Input FASTA file.')
    arg('output_fasta', metavar='OUTPUT_FASTA',
        help='Output FASTA file with sequences aftere rehashing.')

def main(args):
    records = []
    for record in SeqIO.parse(args.input_fasta, 'fasta'):
        id = unique_name(record.id,str(record.seq))
        record.id = id
        record.name = id
        record.description = id
        records.append(record)
    SeqIO.write(records,args.output_fasta, 'fasta-2line')
    logger.info('Rehashed %s sequences', len(records))
