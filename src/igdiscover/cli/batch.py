"""
Execute a batch of IgDiscover commands with multiprocessing.  

First we find all target directories, then execute the subcommand 
using one subprocess per target directory. 

Subcommands are executed inside of IgDiscover folders, so path arguments
for the subcommands should be given relative to the IgDiscover folders. 
"""
import subprocess
import fileinput
from multiprocessing import Pool, get_logger
import logging
from shutil import copyfile
import sys
import pandas as pd
import re
import pkgutil
import importlib
import igdiscover.cli as cli_package
import os
    
logger = logging.getLogger(__name__)

def add_arguments(parser):
    subparsers = parser.add_subparsers()
    batch_init_parser = subparsers.add_parser('init', help='Init IgDiscover.')
    arg = batch_init_parser.add_argument
    arg('--igdiscover-yaml', type=str, default=None,
        help='Path to igdiscover.yaml file to use for the runs.')
    arg('--dry-run', action='store_true', default=False,
        help='Print the commands instead of running them.')
    arg('--read-1-patterns', type=str, default='_1,R1,reads.1',
        help='Read filename substrings which uniquely identify read 1 of a pair of paired end sequence files.')
    arg('--database', '--db', default=None,
        help='Directory with human_gl.aux, V.fasta, D.fasta, J.fasta, V.csv and order.fasta files.')
    arg('library_dir', type=str,
        help='Directory containing sequence libraries to run. '
        'Expects subfolders of library_dir to contain one or two fasta or fastq files. '
        'Directory structure of library_dir will be recreated in run_dir')
    arg('run_dir', type=str,
        help='Directory to place IgDiscover run folders. Run folders '
        'will be named identically to library folders, recreating the directory tree in library_dir.')

    batch_igdiscover_parser = subparsers.add_parser('igdiscover', help='Init and run igdiscover in one step.')
    arg = batch_igdiscover_parser.add_argument
    arg('--igdiscover-yaml', type=str, default=None,
        help='Path to igdiscover.yaml file to use for the runs.')
    arg('--dry-run', action='store_true', default=False,
        help='Print the commands instead of running them.')
    arg('--num-pooled', type=int, default=4,
        help='Number of IgDiscover runs to run simultaneously.')
    arg('--read-1-patterns', type=str, default='_1,R1,reads.1',
        help='Read filename substrings which uniquely identify read 1 of a pair of paired end sequence files.')
    arg('--database', '--db', default=None,
        help='Directory with human_gl.aux, V.fasta, D.fasta, J.fasta, V.csv and order.fasta files.')
    arg('library_dir', type=str,
        help='Directory containing sequence libraries to run. '
        'Expects subfolders of library_dir to contain one or two fasta or fastq files. '
        'Directory structure of library_dir will be recreated in run_dir')
    arg('run_dir', type=str,
        help='Directory to place IgDiscover run folders. Run folders '
        'will be named identically to library folders, recreating the directory tree in library_dir.')

    batch_collect_parser = subparsers.add_parser('collect', help='Concatenate a delimited file '
                                            'from a batch of runs, adding a column that indicates run folder name.')
    arg = batch_collect_parser.add_argument
    arg('--file', type=str, default="iteration-01/new_V_germline.tab",
        help='Path to file you want to collect relative to the individual run folders. For example, '
        'collect new_V_germline tabs by writing iteration-01/new_V_germline.tab.')
    arg('--separator', type=str, default="\t",
        help='Field separator for the file you want to collect.')
    arg('--force', action='store_true',
        help='Collect even if some runs are missing the file.')
    arg('run_dir', type=str,
        help='Directory containing IgDiscover run folders. '
             'All subdirectories containing an igdiscover.yaml file are considered IgDiscover run folders.')
    arg('outpath', type=str,
        help='Tab delimited file where to put the collected results')
    
    
    # add params
    # some commands don't make sense to run per IgDiscover folder, so exclude them 
    exclude_modules = ["batch", "init", "sra"]
    modules = list(set([el[1] for el in pkgutil.iter_modules(cli_package.__path__)]).difference(set(exclude_modules)))
    for subcommand in modules:
        module = importlib.import_module("." + subcommand, cli_package.__name__)
        subparser = subparsers.add_parser(subcommand,
            help=module.__doc__.split('\n')[1], description=module.__doc__)
        arg = subparser.add_argument
        arg('--num-pooled', type=int, default=4,
            help='Number of module runs to execute simultaneously.')
        arg('--dry-run', action='store_true', default=False,
            help='Print the commands instead of running them.')
        arg('--stdout', type=str, default="batch_stdout.txt",
            help='Redirect stdout to this path. Useful for commands which output to stdout, like igblast and clonotypes.')
        arg('--stderr', type=str, default="batch_stderr.txt",
            help='Redirect stdout to this path. This will capture the logger output from the subcommand.')
        
        module.add_arguments(subparser)
        
        arg('run_dir', type=str,
            help='Directory containing IgDiscover run folders. '
                 'All subdirectories containing an igdiscover.yaml file are considered IgDiscover run folders.')
        
    if len(sys.argv) == 2 and sys.argv[1] == "batch":
        parser.print_help()
        sys.exit(1)
        
def igdiscover(args, init_only = False):
    read_1_patterns = args.read_1_patterns.split(",")
    read_1_regex = ".*" + ".*|.*".join(read_1_patterns) + ".*"
    dirs_to_create = []
    target_list = []
    init_commands = []
    for el in os.walk(args.library_dir):
        seq_files = [file for file in el[2] if "fasta" in file or "fastq" in file]
        # skip directories that don't have sequence files or have more than 2
        if 0 < len(seq_files) <= 2:
            d = el[0]
            suffix = os.path.relpath(d, args.library_dir)
            # folder root which needs to be created in run_dir
            out_root = os.path.join(args.run_dir, os.path.split(suffix)[0])
            # full igdiscover folder path
            out_folder = os.path.join(args.run_dir, suffix)
            
            #single end reads
            if len(seq_files) == 1:
                init_command = " ".join(["igdiscover", "init",
                            "--db", args.database,
                            "--single-reads", os.path.join(d, seq_files[0]),
                            out_folder])

            #paired end reads
            if len(seq_files) == 2:
                ind = [i for i in range(len(seq_files)) if re.match(read_1_regex, seq_files[i])]
                if len(ind) != 1:
                    logger.error("Could not determine read 1 file between " + str(seq_files) + " in " + d)
                else:
                    init_command = " ".join(["igdiscover", "init", "--db", args.database,
                                 "--reads1", os.path.join(d, seq_files[ind[0]]), out_folder])

            dirs_to_create.append(out_root)
            target_list.append(os.path.abspath(out_folder))
            init_commands.append(init_command)
    dirs_to_create = pd.unique(dirs_to_create)
    if args.dry_run:
        logger.info("Directories to be created:\n%s\n", "\n".join(dirs_to_create))
        
        logger.info("Init commands to run:\n%s\n", "\n".join(init_commands))
        if not init_only:
            logger.info("IgDiscover directories to run:\n%s\n", "\n".join(target_list))
    else:
        # create necessary directories
        for d in dirs_to_create:
            if not os.path.isdir(d):
                os.makedirs(d)
    
        # init runs
        init_module = importlib.import_module(".init", cli_package.__name__)
        for init_command in init_commands:
            subprocess.call(init_command, shell = True)
        
        # use yaml file if given
        if args.igdiscover_yaml:
            for out_folder in target_list:
                copyfile(args.igdiscover_yaml, os.path.join(out_folder, 'igdiscover.yaml'))
        
        if not init_only:
            # start IgDiscover runs
            p = Pool(args.num_pooled)
            dicts = [{"command" : "nohup igdiscover run", "directory" : target} for target in target_list]
            p.map(runner, dicts)

    logger.info("Batch IgDiscover runs completed.")


def collect(args):
    to_collect_dirs = get_input_dirs(args.run_dir, args.file, args.force)
    cases = ["_".join(os.path.relpath(d, args.run_dir).split("/")) for d in to_collect_dirs]        
    f = to_collect_dirs[0]
    temp = pd.read_csv(os.path.join(to_collect_dirs[0], args.file), sep = args.separator)
    temp['case'] = cases[0]
    temp.to_csv(args.outpath, sep = '\t', mode = 'w', index = False, header = True)
    for i in range(1, len(to_collect_dirs)):
        temp = pd.read_csv(os.path.join(to_collect_dirs[i], args.file), sep = args.separator)
        temp['case'] = cases[i]
        temp.to_csv(args.outpath, sep = '\t', mode = 'a', index = False, header = False)
    
    logger.info("Batch collection completed.")
    
def get_input_dirs(directory, relative_path, force):
    """
    Get all IgDiscover subdirectories in directory argument which contain relative_path for collect. 
    If some IgDiscover directories do not contain relative_path and force is False, error out. 
    """
    igdiscover_run_dirs = get_igdiscover_run_dirs(directory)
    res_subdirs = [os.path.abspath(igdiscover_run_dir) for igdiscover_run_dir in igdiscover_run_dirs if os.path.isfile(os.path.join(igdiscover_run_dir, relative_path))]
    # check for runs missing the file
    if not force:
        dirs_without_file = list(set(igdiscover_run_dirs).difference(set(res_subdirs)))
        if len(dirs_without_file) > 0:
            logger.error("You are missing file(s) %s. Use --force option to ignore", ", ".join([os.path.join(d, relative_path) for d in dirs_without_file]))
            sys.exit(1)
    
    if len(res_subdirs) == 0:
        logger.error("None of the IgDiscover directories contain the file! IgDiscover directories found:\n%s\n", "\n".join(igdiscover_run_dirs))
        sys.exit(1)       
        
    return res_subdirs

def get_igdiscover_run_dirs(directory):
    return [os.path.abspath(el[0]) for el in os.walk(directory) if os.path.isfile(os.path.join(el[0], "igdiscover.yaml"))]

def get_command_str(args, batch_value_args, batch_novalue_args):
    """
    Generate the module command that batch will be running.
    I opted for this instead of calling module.main because 
    I wanted to easily capture logging outputs from multiple processes with nohup. 
    """
    argv = sys.argv
    # remove run_dir and everything after it
    i = [i for i in range(len(argv)) if argv[i] == args.run_dir][0]
    argv = sys.argv[:i]
    
    # remove everything before batch subcommand
    i = [i for i in range(len(argv)) if "batch" in argv[i]][0]
    argv = argv[i + 1:]
    argv.insert(0, "igdiscover")
    
    # where to redirect stdout
    stdout_str = ""
    if args.stdout: 
        stdout_str = " > " + args.stdout
        
    # where to redirect stderr/logger output
    stderr_str = ""
    if args.stderr: 
        stderr_str = " 2> " + args.stderr
    def index_no_error(arr, el):
        try:
             return arr.index(el)
        except ValueError:
             return -1
            
    # remove arguments for batch module itself
    for arg in batch_value_args:
        i = index_no_error(argv, arg)
        if i != -1:
            del argv[i:i+2]
            
    for arg in batch_novalue_args:
        i = index_no_error(argv, arg)
        if i != -1:
            del argv[i]
              
    command_str = " ".join(argv) + stdout_str + stderr_str
    return command_str
    
def runner(d):
    logger.info("Running in %s", d["directory"])
    os.chdir(d["directory"])
    subprocess.call(d["command"], shell = True)

def main(args):
    # args for batch module need to be removed before subcommand call
    # we need to distinguish between args with and without values
    batch_value_args = ["--num-pooled", "--stdout", "--stderr"]
    batch_novalue_args = ["--dry-run"]

    subcommand = sys.argv[2]
    if subcommand == "igdiscover":
        igdiscover(args)
    elif subcommand == "collect":
        collect(args)
    # special case because I can't use igdiscover.yaml to detect where to run
    elif subcommand == "init":
        igdiscover(args, init_only = True)
    else:
        igdiscover_run_dirs = get_igdiscover_run_dirs(args.run_dir)
        command_str = get_command_str(args, batch_value_args, batch_novalue_args)
        if args.dry_run:
            logger.info("IgDiscover directories found:\n%s\n", "\n".join(igdiscover_run_dirs))
            logger.info("Command to run in each IgDiscover directory:\n%s\n", command_str)
        else:
            dicts = [{"command" : command_str, "directory" : igdiscover_dir} for igdiscover_dir in igdiscover_run_dirs]
            p = Pool(args.num_pooled)
            p.map(runner, dicts)
