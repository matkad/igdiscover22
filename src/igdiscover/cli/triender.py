"""
Find candidate V end variants which IgDiscover may have missed due to clustering only up to the CDR3.
Requires query V sequences which are suspected to have undetected end variants.

First, find all sequences with an exact match to the query in a clonally collapsed assignment file 'trim' nucleotides trimmed off. 
Next, use the last 'trim' nucleotides of those sequences to build a prefix trie whose nodes count the number of times each nucleotide was reached. 
Finally, get a list of possible suffixes (end variants) by descending the trie until the minimum count, ind-prop, or tot-prop conditions are failed.
"""

import logging
import networkx as nx
import queue
import os
import dnaio
import pandas as pd
import numpy as np
from collections import Counter
from ..table import read_table

logger = logging.getLogger(__name__)

def add_arguments(parser):
    arg = parser.add_argument
    arg("--ind-prop", type = float, default = 0.2, help = "The minimum ratio of child to parent node count required to continue down the trie.")
    arg("--tot-prop", type = float, default = 0.05, help = "The minimum ratio of node to total number of suffixes required to continue down the trie.")
    arg("--mincount", type = float, default = 10, help = 'The minimum count needed to continue traversing down the trie.')
    arg("--trim", type = int, default = 8, help = "Number of nucleotides to trim off of the query sequences before inferring suffixes.")
    arg("--db", type = str, help = "If provided, the output will include an additional column 'v_call' indicating the name of the inferred allele if it is in the database")
    arg("--verbose", action = 'store_true')

    arg("assignments", help = "Assignment file from which to infer end variants.")
    arg("queries", help = "Fasta file with query sequences to trim and infer suffixes for. For example, a final/database/V.fasta file.")
    arg("output", help = "Where to write triender output.")

"""
Class for storing a sequence prefix trie which allows traversal to find candidate end variants
"""
class c_trie:
    def __init__(self):
        self.g = nx.DiGraph()
        self.g.add_node(0)
        self.leafs = []
        self.n_nodes = 1
        self.g.nodes[0]['nuc'] = ""
        self.g.nodes[0]['ct'] = 0
                

    def get_strs(self, inds):
        """
        Travel upwards from a list of node indices, and return the strings they represent
        """
        suffix = []
        for ind in inds:
            string = ""
            curr = ind
            while curr != 0:
                string += self.g.nodes[curr]['nuc']
                curr = list(self.g.predecessors(curr))[0]
            suffix.append(string[::-1])
        return suffix
    
    

    def conditional_bf_traversal(self, ind_prop = .25, tot_prop = .1, mincount = 10):
        """
        Breadth first traversal under the condition that each node has ind_prop * counts of its parent and
        tot_prop * total counts. 
        """
        qq = queue.Queue()
        qq.put(0)
        res = []
        #root count to weed out variants that are rare overall
        ref_i = self.g.nodes[0]['ct']
        while not qq.empty():
            v = qq.get()
            ref_ct = self.g.nodes[v]['ct'] if v != 0 else len(self.leafs)
            #check if node has children 
            succs = list(self.g.successors(v))
            
            if len(succs) == 0:
                res.append(v)
            else:
                cts = [self.g.nodes[i]['ct'] for i in succs]
                #find successors which meet the criteria
                quality_succs = [succs[i] for i in range(len(cts)) if cts[i] > ind_prop * ref_ct and cts[i] > tot_prop * ref_i and cts[i] >= mincount]
                if len(quality_succs) == 0:
                    res.append(v)
                else:
                    for k in quality_succs:
                        qq.put(k)

        return res
    
 
    def insert(self, seq, allele, ct):
        """
        Insert a new sequence. Its end node will be marked with  allele name. Each time a node is used, its count
        increases by one - signaling one more use of that prefix
        """       
        curr = 0

        #go through nucs in seq
        for i in range(len(seq)):
            #see if there is a successor with that nucleotide
            arr = [k for k in self.g.successors(curr)]
            res = find([self.g.nodes[k]['nuc'] for k in arr], seq[i])
            #if there is, continue down and add count
            if res is not None:
                curr = arr[res]
                self.g.nodes[curr]['ct'] += 1
            #if there isn't, add node 
            else:
                self.g.add_node(self.n_nodes)
                self.g.nodes[self.n_nodes]['ct'] = 1
                self.g.nodes[self.n_nodes]['nuc'] = seq[i]
                self.g.add_edge(curr, self.n_nodes)
                curr = self.n_nodes
                self.n_nodes += 1
                
            if i == len(seq) - 1:
                if 'allele' in self.g.nodes[curr]:
                    if allele not in self.g.nodes[curr]['allele']:
                        self.g.nodes[curr]['allele'].append(allele)
                else:
                    self.g.nodes[curr]['allele'] = [allele]
                
                self.leafs.append(curr)
            
def find(arr, item):
    """
    Find index of an item in array
    """
    for i in range(len(arr)):
        if arr[i] == item:
            return i
    return None

def safe_divide(x, y):
    return x / y if y != 0 else 0

def triender(assignments, 
            query,
            query_name,
            verbose = False,
            trim = 8,
            ind_prop = .25,
            tot_prop = .1,
            sequence_column = "sequence",
            V_column = "v_call",
            J_column = "j_call",
            CDR3_column = "cdr3",
            mincount = 10):
    """
    Checks if clonally collapsed assignment file contains end variants compared to the reference (query) sequence
    given. Triender finds VDJ sequences that match the query minus trim nucleotides (the stem) and builds
    a prefix tree of the trimmed nucleotides. Triender travels down the prefix tree, recording all possible alleles
    whose counts are above ind_prop of the parent node and tot_prop of the total number of matches for the stem.
    """

    # we only need sequences containing the trim query in the trie
    assignments = assignments.assign(find_ind = assignments[sequence_column].apply(lambda s: s.find(query[:len(query) - trim])))
    assignments = assignments[assignments["find_ind"] != -1].reset_index(drop = True)

    if len(assignments) != 0:

        assignments = assignments.assign(suffix = assignments.apply(lambda row: row[sequence_column][row["find_ind"] + len(query) - trim: row["find_ind"] + len(query)], axis = 1))
    
        # generate trie
        tree = c_trie()
        tree.g.nodes[0]['ct'] = len(assignments)
        for row in assignments.to_dict(orient = "records"):
            tree.insert(row["suffix"], row[V_column], row["count"])

        # travel down tree from root as far as conditions hold. Conditions are that count 
        # is > tot_prop * total count and > ind_prop * parent node count 
        ress = tree.conditional_bf_traversal(ind_prop = ind_prop, tot_prop = tot_prop, mincount = mincount)

        # get all query suffixes by traveling up from nodes reached  
        suffix = tree.get_strs(ress)
        
        # Allelic frequencies
        counts = [tree.g.nodes[i]['ct'] for i in ress]
        allele_frequency = [safe_divide(c, len(assignments)) for c in counts]

        diffs = [[] for k in range(len(suffix))]

        # TODO: should we determine this during the traversal?
        # thru all trie traversals
        for i in range(len(suffix)):
            # thru len of current traversal
            for q in range(len(suffix[i])):
                # check for differences compared to query
                if query[q + len(query) - trim] != suffix[i][q]:
                    diffs[i].append((q + len(query) - trim, query[q + len(query) - trim], suffix[i][q]))
                    
        
        # quantify the diversity of CDR3 lengths for each query + possible suffix to weed out clonal expansions
        full_seqs = [query[:len(query) - trim] + strr for strr in suffix]
        CDR3_freqs = [dict() for i in range(len(full_seqs))]
        
        # quantify the j diversity for every full sequence to weed out clonal expansions
        js = [dict() for i in range(len(full_seqs))]
        num_js = [0 for i in range(len(full_seqs))]

        def maxfreq_CDR3(df):
            # pandas groupby sometimes creates empty groups - figure out why this is the case
            if len(df) > 0:
                ctr = Counter(df[CDR3_column].values)
                o = pd.DataFrame([{CDR3_column : key, "count" : ctr[key]} for key in ctr])
                return pd.DataFrame([{"suffix_index" : int(df['suffix_index'].values[0]), "maxfreq_CDR3" : o['count'].max() / o['count'].sum()}])
            else:
                return pd.DataFrame([{"suffix_index" : [], "maxfreq_CDR3": []}])
        
        def get_suffix(row, suffix):
            arr = [i for i in range(len(suffix)) if suffix[i] in row["suffix"]]
            if len(arr) == 0:
                return np.nan
            else: 
                return arr[0]
        assignments = assignments.assign(suffix_index = assignments.apply(lambda row: get_suffix(row, suffix), axis = 1))
        select = ~assignments.suffix_index.isna()
        if verbose:
            logger.info(f"Found {str(assignments.suffix_index.nunique())} alleles from query {query_name}") 
        if select.sum() > 0:

            unique_j_df = assignments[select].groupby(by = "suffix_index", as_index = False).nunique().loc[:,["suffix_index", J_column]]
            unique_j_arr = unique_j_df.sort_values(by = "suffix_index")[J_column].values

            maxfreq_CDR3_df = assignments[select].reset_index(drop = True).groupby(by = "suffix_index", as_index = False).apply(maxfreq_CDR3)
            maxfreq_CDR3_arr = maxfreq_CDR3_df.sort_values(by = "suffix_index")["maxfreq_CDR3"].values
            output = pd.DataFrame({'query' : [query_name for i in range(len(full_seqs))], 
                            "query_len" : len(query),
                            'count_ratio' : allele_frequency,
                            'count' : counts,
                            'seq_maxfreq_CDR3' : maxfreq_CDR3_arr,
                            "Js" : unique_j_arr,
                            "query_diff" : diffs,
                            "query_trim" : trim,
                            "suffix" : suffix,
                            "sequence" : full_seqs,
                            "n_suffixes" : len(full_seqs)})
            return output
        else:
            return pd.DataFrame()
    else:
        logger.info(f"None of the sequences matched the query {query_name} with {trim} nucleotides trimmed off.")
        return pd.DataFrame()

def main(args):
    assignments = read_table(args.assignments)
    results = pd.DataFrame()
    with dnaio.open(args.queries) as fr:
        for record in fr:
            temp = triender(assignments, record.sequence, record.name, 
                verbose = args.verbose, ind_prop = args.ind_prop,
                tot_prop = args.tot_prop, trim = args.trim, mincount = args.mincount)
            results = pd.concat([results, temp])

    results = results.reset_index(drop = True)
    if args.db != None: 
        with dnaio.open(args.db) as fr:
            db_df = pd.DataFrame([{"v_call" : record.name, "sequence" : record.sequence} for record in fr]).drop_duplicates(subset = "sequence")
            results = pd.merge(results, db_df, on = "sequence", how = "left")

    results.to_csv(args.output, sep = '\t', index = False)
