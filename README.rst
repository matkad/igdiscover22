.. image:: https://gitlab.com/gkhlab/igdiscover22/badges/main/pipeline.svg?style=flat
    :target: https://gitlab.com/gkhlab/igdiscover22/-/pipelines


.. image:: https://img.shields.io/badge/docs-latest-blue.svg
    :target: https://gkhlab.gitlab.io/igdiscover22


.. image:: https://gitlab.com/gkhlab/igdiscover22/badges/main/coverage.svg?style=flat

==========
IgDiscover
==========

IgDiscover analyzes antibody repertoires and discovers new V genes from high-throughput sequencing reads.
Heavy chains, kappa and lambda light chains are supported (to discover VH, VK and VL genes).

IgDiscover is the result of a collaboration between the `Gunilla Karlsson Hedestam group <http://ki.se/en/mtc/gunilla-karlsson-hedestam-group>`_
at the `Department of Microbiology, Tumor and Cell Biology <http://ki.se/en/mtc/>`_ at `Karolinska Institutet <http://ki.se/en/>`_,
Sweden and the `Bioinformatics Long-Term Support <https://www.scilifelab.se/facilities/wabi/>`_ facility
at `Science for Life Laboratory (SciLifeLab) <https://www.scilifelab.se/>`_, Sweden.

If you use IgDiscover, please cite:

    | Corcoran, Martin M. and Phad, Ganesh E. and Bernat, Néstor Vázquez and Stahl-Hennig,
      Christiane and Sumida, Noriyuki and Persson, Mats A.A. and Martin, Marcel and
      Karlsson Hedestam, Gunilla B.
    | *Production of individualized V gene databases reveals high levels of immunoglobulin genetic
      diversity.*
    | Nature Communications 7:13642 (2016)
    | https://dx.doi.org/10.1038/ncomms13642


Links
-----

* `Documentation <https://gkhlab.gitlab.io/igdiscover22>`_
* `Source code <https://gitlab.com/gkhlab/igdiscover22/>`_
* `Report an issue <https://gitlab.com/gkhlab/igdiscover22/-/issues>`_

|

.. image:: https://gitlab.com/gkhlab/igdiscover22/-/raw/main/doc/clusterplot.jpeg

|

